<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.


if ( ! function_exists( 'sp_eap_admin_enqueue_scripts' ) ) {
	/**
	 *
	 * Framework admin enqueue style and scripts
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @return void
	 */
	function sp_eap_admin_enqueue_scripts() {
		$current_screen        = get_current_screen();
		$the_current_post_type = $current_screen->post_type;

		if ( 'sp_easy_accordion' === $the_current_post_type ) {
			// Admin utilities.
			wp_enqueue_media();

			// wp core styles.
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'wp-jquery-ui-dialog' );
			$prefix = defined( 'WP_DEBUG' ) && WP_DEBUG ? '' : '.min';
			// Framework core styles.
			wp_enqueue_style( 'sp-eap-framework', SP_EA_URL . 'admin/views/meta-box/assets/css/sp-framework' . $prefix . '.css', array(), SP_EA_VERSION, 'all' );
			wp_enqueue_style( 'sp-eap-custom', SP_EA_URL . 'admin/views/meta-box/assets/css/sp-custom' . $prefix . '.css', array(), SP_EA_VERSION, 'all' );
			wp_enqueue_style( 'eap-style', SP_EA_URL . 'admin/views/meta-box/assets/css/sp-style.css', array(), SP_EA_VERSION, 'all' );
			wp_enqueue_style( 'eap-font-awesome', SP_EA_URL . 'public/assets/css/font-awesome.min.css', array(), SP_EA_VERSION, 'all' );
			if ( is_rtl() ) {
				wp_enqueue_style( 'sp-framework-rtl', SP_EA_URL . 'admin/views/meta-box/assets/css/sp-framework-rtl' . $prefix . '.css', array(), SP_EA_VERSION, 'all' );
			}

			// wp core scripts.
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'jquery-ui-dialog' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'jquery-ui-accordion' );
			// framework core scripts.
			wp_enqueue_script( 'sp-eap-dependency', SP_EA_URL . 'admin/views/meta-box/assets/js/dependency.js', array( 'jquery' ), SP_EA_VERSION, true );
			wp_enqueue_script( 'sp-eap-plugins', SP_EA_URL . 'admin/views/meta-box/assets/js/sp-plugins' . $prefix . '.js', array(), SP_EA_VERSION, true );
			wp_enqueue_script( 'sp-eap-framework', SP_EA_URL . 'admin/views/meta-box/assets/js/sp-framework' . $prefix . '.js', array( 'sp-eap-plugins' ), SP_EA_VERSION, true );
			wp_localize_script(
				'sp-eap-framework', 'sp_local', array(
					'pluginsUrl' => SP_EA_URL,
				)
			);
		}
	}
	add_action( 'admin_enqueue_scripts', 'sp_eap_admin_enqueue_scripts' );
}
