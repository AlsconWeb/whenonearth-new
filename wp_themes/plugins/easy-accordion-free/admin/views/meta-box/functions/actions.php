<?php
/**
 * The actions of the sp framework.
 *
 * @package SP Framework.
 */

if ( ! defined( 'ABSPATH' ) ) {
	die; } // Cannot access pages directly.

if ( ! function_exists( 'sp_get_icons' ) ) {
	/**
	 * Get icons from admin ajax
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 */
	function sp_get_icons() {
		do_action( 'sp_add_icons_before' );

		$jsons = glob( SP_EAP_F_DIR . '/fields/icon/*.json' );

		if ( ! empty( $jsons ) ) {

			foreach ( $jsons as $path ) {

				$object = sp_get_icon_fonts( 'fields/icon/' . basename( $path ) );

				if ( is_object( $object ) ) {

					echo ( count( $jsons ) >= 2 ) ? '<h4 class="sp-icon-title">' . $object->name . '</h4>' : '';

					foreach ( $object->icons as $icon ) {
						echo '<a class="sp-icon-tooltip" data-icon="' . $icon . '" data-title="' . $icon . '"><span class="sp-icon sp-selector"><i class="fa ' . $icon . '"></i></span></a>';
					}
				} else {
					echo '<h4 class="sp-icon-title">' . __( 'Error! Can not load json file.', 'easy-accordion-free' ) . '</h4>';
				}
			}
		}

		do_action( 'sp_add_icons' );
		do_action( 'sp_add_icons_after' );

		die();
	}
	add_action( 'wp_ajax_sp-get-icons', 'sp_get_icons' );
}

if ( ! function_exists( 'sp_export_options' ) ) {
	/**
	 *
	 * Export options
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 */
	function sp_export_options() {

		header( 'Content-Type: plain/text' );
		header( 'Content-disposition: attachment; filename=backup-options-' . gmdate( 'd-m-Y' ) . '.txt' );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Pragma: no-cache' );
		header( 'Expires: 0' );

		echo sp_encode_string( get_option( SP_EAP_OPTION ) );

		die();
	}
	add_action( 'wp_ajax_sp-export-options', 'sp_export_options' );
}

if ( ! function_exists( 'sp_pro_set_icons' ) ) {
	/**
	 *
	 * Set icons for wp dialog
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 */
	function sp_pro_set_icons() {

		echo '<div id="sp-icon-dialog" class="sp-dialog" title="' . __( 'Add Icon', 'easy-accordion-free' ) . '">';
		echo '<div class="sp-dialog-header sp-text-center"><input type="text" placeholder="' . __( 'Search a Icon...', 'easy-accordion-free' ) . '" class="sp-icon-search" /></div>';
		echo '<div class="sp-dialog-load"><div class="sp-icon-loading">' . __( 'Loading...', 'easy-accordion-free' ) . '</div></div>';
		echo '</div>';

	}
	add_action( 'admin_footer', 'sp_pro_set_icons' );
	add_action( 'customize_controls_print_footer_scripts', 'sp_pro_set_icons' );
}

/**
 * Get post types to the select box.
 *
 * @return void
 */
function eap_get_posts() {
	extract( $_REQUEST );
	$all_posts = get_posts(
		array(
			'post_type'      => $post_type,
			'posts_per_page' => -1,
		)
	);
	foreach ( $all_posts as $key => $post_obj ) {
		echo '<option value="' . $post_obj->ID . '">' . $post_obj->post_title . '</option>';
	}
	die( 0 );
}
add_action( 'wp_ajax_eap_get_posts', 'eap_get_posts' );

/**
 * Populate the taxonomy name list to he select option.
 *
 * @return void
 */
function eap_get_taxonomies() {
	extract( $_REQUEST );
	$taxonomy_names = get_object_taxonomies( array( 'post_type' => $post_type ), 'names' );
	foreach ( $taxonomy_names as $key => $label ) {
		echo '<option value="' . $label . '">' . $label . '</option>';
	}
	die( 0 );
}
add_action( 'wp_ajax_eap_get_taxonomies', 'eap_get_taxonomies' );

/**
 * Populate the taxonomy terms list to the select option.
 *
 * @return void
 */
function eap_get_terms() {
	extract( $_REQUEST );
	$terms = get_terms( $post_taxonomy );
	foreach ( $terms as $key => $value ) {
		echo '<option value="' . $value->term_id . '">' . $value->name . '</option>';
	}
	die( 0 );
}
add_action( 'wp_ajax_eap_get_terms', 'eap_get_terms' );
