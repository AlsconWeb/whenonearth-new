<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
}
// Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

/**
 * Accordion types Metabox.
 */
$options[] = array(
	'id'        => 'sp_eap_upload_options',
	'title'     => __( 'Upload Options', 'easy-accordion-free' ),
	'post_type' => 'sp_easy_accordion',
	'context'   => 'normal',
	'priority'  => 'default',
	'sections'  => array(
		// Begin: a section.
		array(
			'name'   => 'sp_eap_upload_options_1',
			// 'title'  => __( 'Accordion Content', 'easy-accordion-free' ),
			'icon'   => 'fa fa-file',
			// Begin fields.
			'fields' => array(
				array(
					'id'         => 'eap_accordion_type',
					'type'       => 'accordion_type',
					'title'      => __( 'Accordion From', 'easy-accordion-free' ),
					'options'    => array(
						'content-accordion' => array(
							'text'     => __( 'Accordion', 'easy-accordion-free' ),
							'pro_only' => true,
						),
						'post-accordion'    => array(
							'text'     => __( 'Post Types (Pro)', 'easy-accordion-free' ),
							'pro_only' => true,
						),
					),
					'radio'      => true,
					'default'    => 'content-accordion',
					'attributes' => array(
						'data-depend-id' => 'eap_accordion_type',
					),
				),
				// Content Accordion.
				array(
					'id'              => 'accordion_content_source',
					'type'            => 'group',
					'title'           => __( 'Accordion', 'easy-accordion-free' ),
					'button_title'    => 'Add Accordion',
					'wrap_class'      => 'eap_accordion_content_wrapper',
					'accordion_title' => 'Item',
					'fields'          => array(
						array(
							'id'         => 'accordion_content_title',
							'type'       => 'text',
							'wrap_class' => 'eap_accordion_content_source',
							'title'      => __( 'Title', 'easy-accordion-free' ),
						),
						array(
							'id'         => 'accordion_content_description',
							'type'       => 'wysiwyg',
							'wrap_class' => 'eap_accordion_content_source',
							'title'      => __( 'Description', 'easy-accordion-free' ),
							'settings'   => array(
								'textarea_rows' => 10,
								'tinymce'       => true,
								'media_buttons' => false,
							),
						),
					),
					'dependency'      => array( 'eap_accordion_type', '==', 'content-accordion' ),
				), // End of Content Accordion.

			), // End: fields.
		), // End: Upload section.
	),
);
// -----------------------------------------
// Shortcode Generator Options
// -----------------------------------------
$options[] = array(
	'id'        => 'sp_eap_shortcode_options',
	'title'     => __( 'Shortcode Options', 'easy-accordion-free' ),
	'post_type' => 'sp_easy_accordion',
	'context'   => 'normal',
	'priority'  => 'default',
	'sections'  => array(
		// Begin: a section.
		array(
			'name'   => 'sp_eap_shortcode_option_1',
			'title'  => __( 'Accordion Settings', 'easy-accordion-free' ),
			'icon'   => 'fa fa-wrench',
			// Begin fields.
			'fields' => array(
				array(
					'id'         => 'eap_accordion_layout',
					'type'       => 'image_select',
					'title'      => __( 'Accordion Layout', 'easy-accordion-free' ),
					'desc'       => __( 'Choose an accordion layout.', 'easy-accordion-free' ),
					'options'    => array(
						'vertical'   => array(
							'image' => SP_EA_URL . 'admin/views/meta-box/assets/images/vertical.png',
						),
						'horizontal' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/horizontal.jpg',
							'pro_only' => true,
						),
					),
					'attributes' => array(
						'data-depend-id' => 'eap_accordion_layout',
					),
					'radio'      => true,
					'default'    => 'vertical',
				),
				array(
					'id'         => 'eap_accordion_theme',
					'type'       => 'theme_select',
					'title'      => __( 'Choose a Theme', 'easy-accordion-free' ),
					'desc'       => __( 'Select an accordion theme style. 16+ Premium Themes available in <a href="https://shapedplugin.com/demo/easy-accordion-pro/" target="_blank">pro version!</a> ', 'easy-accordion-free' ),
					'options'    => array(
						'sp-ea-one' => array(
							'text' => __( 'Theme One', 'easy-accordion-free' ),
						),
						'sp-ea-two' => array(
							'text'     => __( '16+ Themes (Pro)', 'easy-accordion-free' ),
							'pro_only' => true,
						),
					),
					'default'    => 'sp-ea-one',
					'attributes' => array(
						'data-depend-id' => 'eap_accordion_theme',
					),
				),

				array(
					'id'      => 'eap_accordion_mode',
					'type'    => 'radio',
					'title'   => __( 'Accordion Mode', 'easy-accordion-free' ),
					'desc'    => __( 'Expand or collapse accordion option on page load.', 'easy-accordion-free' ),
					'options' => array(
						'ea-first-open' => array(
							'text' => __( 'First Open', 'easy-accordion-free' ),
						),
						'ea-multi-open' => array(
							'text' => __( 'All Open', 'easy-accordion-free' ),
						),
						'ea-all-close'  => array(
							'text' => __( 'All Folded', 'easy-accordion-free' ),
						),
					),
					'default' => 'ea-first-open',
				),
				array(
					'id'      => 'eap_mutliple_collapse',
					'type'    => 'checkbox',
					'title'   => __( 'Collapsible', 'easy-accordion-free' ),
					'desc'    => __( 'Check to open multiple accordions together.', 'easy-accordion-free' ),
					'default' => false,
				),

				array(
					'id'      => 'eap_accordion_event',
					'type'    => 'radio',
					'title'   => __( 'Activator Event', 'easy-accordion-free' ),
					'desc'    => __( 'Select event click or mouse over to expand accordion.', 'easy-accordion-free' ),
					'options' => array(
						'ea-click' => array(
							'text' => __( 'Click', 'easy-accordion-free' ),
						),
						'ea-hover' => array(
							'text' => __( 'Mouse Over', 'easy-accordion-free' ),
						),
					),
					'default' => 'ea-click',
				),
				array(
					'id'         => 'eap_accordion_fillspace',
					'type'       => 'checkbox',
					'title'      => __( 'Fixed Content Height', 'easy-accordion-free' ),
					'desc'       => __( 'Check to display collapsible accordion content in a limited amount of space.', 'easy-accordion-free' ),
					'default'    => false,
					'dependency' => array( 'eap_accordion_layout', '==', 'vertical' ),
				),
				array(
					'id'         => 'eap_accordion_fillspace_height',
					'type'       => 'number',
					'title'      => __( 'Maximum Height', 'easy-accordion-free' ),
					'desc'       => __( 'Set fixed accordion content panel height. Defualt height 200px.', 'easy-accordion-free' ),
					'after'      => __( '(px)', 'easy-accordion-free' ),
					'default'    => 200,
					'dependency' => array( 'eap_accordion_layout|eap_accordion_fillspace', '==|==', 'vertical|true' ),
				),
				array(
					'id'      => 'eap_preloader',
					'type'    => 'switcher',
					'title'   => __( 'Preloader', 'easy-accordion-pro' ),
					'desc'    => __( 'Accordion will be hidden until page load completed.', 'easy-accordion-pro' ),
					'default' => false,
				),
			), // End: fields.
		), // End: Accordion section.
		// Begin Style Settings.
		array(
			'name'   => 'sp_eap_shortcode_option_3',
			'title'  => __( 'Display Settings', 'easy-accordion-free' ),
			'icon'   => 'fa fa-paint-brush',
			'fields' => array(
				array(
					'id'      => 'section_title',
					'type'    => 'switcher',
					'title'   => __( 'Accordion Section Title', 'easy-accordion-free' ),
					'desc'    => __( 'Show/hide the accordion section title.', 'easy-accordion-free' ),
					'default' => false,
				),
				array(
					'id'         => 'section_title_margin_bottom',
					'type'       => 'number',
					'title'      => __( 'Accordion  Section Title Margin Bottom', 'easy-accordion-free' ),
					'desc'       => __( 'Set a margin bottom for the accordion section title. Defualt value is 30px.', 'easy-accordion-free' ),
					'after'      => __( '(px)', 'easy-accordion-free' ),
					'default'    => 30,
					'dependency' => array(
						'section_title',
						'==',
						'true',
					),
				),
				array(
					'type'    => 'subheading',
					'content' => __( 'Accordion Expand & Collapse Icons', 'easy-accordion-free' ),
				),
				array(
					'id'      => 'eap_expand_close_icon',
					'type'    => 'switcher',
					'title'   => __( 'Expand and Collapse Icons', 'easy-accordion-free' ),
					'desc'    => __( 'Show/hide expand and collapse Icons.', 'easy-accordion-free' ),
					'default' => true,
				),
				array(
					'id'         => 'eap_expand_collapse_icon',
					'type'       => 'image_select',
					'title'      => __( 'Expand and Collapse Icon Style', 'easy-accordion-free' ),
					'desc'       => __( 'Choose a expand and collapse icon style.', 'easy-accordion-free' ),
					'options'    => array(
						'1'  => array(
							'image' => SP_EA_URL . 'admin/views/meta-box/assets/images/1-plus.png',
						),
						'2'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/2-angle.png',
							'pro_only' => true,
						),
						'3'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/3-angle-double.png',
							'pro_only' => true,
						),
						'4'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/4-arrow.png',
							'pro_only' => true,
						),
						'5'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/5-tick.png',
							'pro_only' => true,
						),
						'6'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/6-chevron.png',
							'pro_only' => true,
						),
						'7'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/7-hand.png',
							'pro_only' => true,
						),
						'8'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/8-carret.png',
							'pro_only' => true,
						),
						'9'  => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/9-agnle-2.png',
							'pro_only' => true,
						),
						'10' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/10-angle-double-up.png',
							'pro_only' => true,
						),
						'11' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/11-arrow-up.png',
							'pro_only' => true,
						),
						'12' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/12-agnle-bold-up.png',
							'pro_only' => true,
						),
						'13' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/13-angle-3.png',
							'pro_only' => true,
						),
						'14' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/14-carret-up.png',
							'pro_only' => true,
						),
						'15' => array(
							'image'    => SP_EA_URL . 'admin/views/meta-box/assets/images/15-angle-double-down.png',
							'pro_only' => true,
						),
					),
					'radio'      => true,
					'default'    => '1',
					'dependency' => array(
						'eap_expand_close_icon',
						'==',
						'true',
					),
				),
				array(
					'id'         => 'eap_icon_size',
					'type'       => 'number',
					'title'      => __( 'Expand and Collapse Icon Size', 'easy-accordion-free' ),
					'desc'       => __( 'Set accordion collapse and expand icon size. Defualt value is 16px.', 'easy-accordion-free' ),
					'after'      => __( '(px)', 'easy-accordion-free' ),
					'default'    => 16,
					'dependency' => array(
						'eap_expand_close_icon',
						'==',
						'true',
					),

				),
				array(
					'id'         => 'eap_icon_color_set',
					'type'       => 'color_picker',
					'title'      => __( 'Icon Color', 'easy-accordion-free' ),
					'desc'       => __( 'Set icon color.', 'easy-accordion-free' ),
					'default'    => '#444',
					'rgba'       => true,
					'dependency' => array(
						'eap_expand_close_icon',
						'==',
						'true',
					),
				),
				array(
					'id'         => 'eap_icon_position',
					'type'       => 'button_set',
					'title'      => __( 'Expand and Collapse Icon Position', 'easy-accordion-free' ),
					'desc'       => __( 'Set accordion expand and collapse icon position or alingment.', 'easy-accordion-free' ),
					'options'    => array(
						'left'  => __( 'Left', 'easy-accordion-free' ),
						'right' => __( 'Right', 'easy-accordion-free' ),
					),
					'radio'      => true,
					'default'    => 'left',
					'dependency' => array(
						'eap_expand_close_icon',
						'==',
						'true',
					),
				),
				array(
					'type'    => 'subheading',
					'content' => __( 'Accordion Item Title & Description', 'easy-accordion-free' ),
				),
				array(
					'id'         => 'eap_border_css',
					'type'       => 'border',
					'title'      => __( 'Item Border', 'easy-accordion-free' ),
					'desc'       => __( 'Set Item border. Defualt value is 1px.', 'easy-accordion-free' ),
					'default'    => array(
						'width' => 1,
						'style' => 'solid',
						'color' => '#e2e2e2',
					),
					'dependency' => array( 'eap_accordion_theme', '!=', 'sp-ea-eleven' ),
				),
				array(
					'id'      => 'eap_title_color',
					'type'    => 'color_picker',
					'title'   => __( 'Title  Color', 'easy-accordion-free' ),
					'desc'    => __( 'Set accordion title color.', 'easy-accordion-free' ),
					'rgba'    => true,
					'default' => '#444',
				),
				array(
					'id'      => 'eap_header_bg_color',
					'type'    => 'color_picker',
					'title'   => __( 'Title Background Color', 'easy-accordion-free' ),
					'desc'    => __( 'Set accordion title background color.', 'easy-accordion-free' ),
					'rgba'    => true,
					'default' => '#eee',
				),
				array(
					'id'      => 'eap_description_color',
					'type'    => 'color_picker',
					'title'   => __( 'Description Color', 'easy-accordion-free' ),
					'desc'    => __( 'Set accordion description color.', 'easy-accordion-free' ),
					'default' => '#444',
					'rgba'    => true,
				),
				array(
					'id'      => 'eap_description_bg_color',
					'type'    => 'color_picker',
					'title'   => __( 'Description Background Color', 'easy-accordion-free' ),
					'desc'    => __( 'Set accordion description background color.', 'easy-accordion-free' ),
					'default' => '#fff',
					'rgba'    => true,
				),
				array(
					'id'      => 'eap_animation_time',
					'type'    => 'number',
					'title'   => __( 'Accordion Transition Time', 'easy-accordion-free' ),
					'desc'    => __( 'Set accordion expand and collapse transition time. Defualt value is 500ms.', 'easy-accordion-free' ),
					'after'   => __( '(millisecond)', 'easy-accordion-free' ),
					'default' => 500,
				),

			), // End Fields.
		), // End a section.
		// Begin Typography the section.
		array(
			'name'   => 'sp_eap_shortcode_option_4',
			'title'  => __( 'Typography', 'easy-accordion-free' ),
			'icon'   => 'fa fa-font',
			// begin: fields.
			'fields' => array(
				array(
					'type'    => 'pronotice',
					'content' => __( 'These Typography (840+ Google Fonts) options are available in the <a href="https://shapedplugin.com/plugin/easy-accordion-pro/" target="_blank">Pro Version</a> only.', 'easy-accordion-free' ),
				),
				array(
					'id'         => 'section_title_font_load',
					'type'       => 'switcher',
					'title'      => __( 'Load Section Title Font', 'easy-accordion-free' ),
					'desc'       => __( 'On/Off google font for the section title.', 'easy-accordion-free' ),
					'default'    => true,
					'dependency' => array(
						'section_title',
						'==',
						'true',
					),
				),
				array(
					'id'           => 'eap_section_title_typography',
					'type'         => 'typography_advanced',
					'title'        => __( 'Section Title Font', 'easy-accordion-free' ),
					'desc'         => __( 'Set Accordion section title font properties.', 'easy-accordion-free' ),
					'default'      => array(
						'family'    => 'Open Sans',
						'variant'   => '500',
						'font'      => 'google',
						'size'      => '28',
						'height'    => '22',
						'alignment' => 'left',
						'transform' => 'none',
						'spacing'   => 'normal',
						'color'     => '#444444',
					),
					'dependency'   => array(
						'section_title',
						'==',
						'true',
					),
					'color'        => true,
					'hover_color'  => false,
					'preview'      => true,
					'preview_text' => 'Accordion Section Title', // Replace preview text with any text you like.
				),
				array(
					'id'      => 'eap_title_font_load',
					'type'    => 'switcher',
					'title'   => __( 'Load Item Title Font', 'easy-accordion-free' ),
					'desc'    => __( 'On/Off google font for the  accordion item title.', 'easy-accordion-free' ),
					'default' => true,
				),
				array(
					'id'           => 'eap_title_typography',
					'type'         => 'typography_advanced',
					'title'        => __( 'Item Title Font', 'easy-accordion-free' ),
					'desc'         => __( 'Set item title font properties.', 'easy-accordion-free' ),
					'default'      => array(
						'family'    => 'Open Sans',
						'variant'   => '600',
						'font'      => 'google',
						'size'      => '20',
						'height'    => '30',
						'alignment' => 'left',
						'transform' => 'none',
						'spacing'   => 'normal',
						'color'     => '#444',
					),
					'color'        => true,
					'preview'      => true,
					'preview_text' => 'Accordion Title', // Replace preview text with any text you like.

				),
				array(
					'id'      => 'eap_desc_font_load',
					'type'    => 'switcher',
					'title'   => __( 'Load Item Description Font', 'easy-accordion-free' ),
					'desc'    => __( 'On/Off google font for the accordion item description.', 'easy-accordion-free' ),
					'default' => true,
				),
				array(
					'id'           => 'eap_content_typography',
					'type'         => 'typography_advanced',
					'title'        => __( 'Item Description Font', 'easy-accordion-free' ),
					'desc'         => __( 'Set item Description font properties.', 'easy-accordion-free' ),
					'default'      => array(
						'family'    => 'Open Sans',
						'variant'   => '500',
						'font'      => 'google',
						'size'      => '16',
						'height'    => '26',
						'alignment' => 'left',
						'transform' => 'none',
						'spacing'   => 'normal',
						'color'     => '#444',
					),
					'color'        => true,
					'preview'      => true,
					'preview_text' => 'The Accordion Description', // Replace preview text with any text you like.
				),
			), // end: fields
		), // end: Typography section
		// end: a section
		array(
			'name'   => 'sp_eap_upgrade_area',
			'title'  => __( 'Upgrade to Pro', 'easy-accordion-free' ),
			'icon'   => ' fa fa-rocket',
			'fields' => array(),
		),
	),
);

SP_EAP_Framework_Metabox::instance( $options );
