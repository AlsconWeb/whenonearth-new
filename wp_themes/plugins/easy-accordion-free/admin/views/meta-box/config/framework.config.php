<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings = array(
	'menu_title'      => __( 'Settings', 'easy-accordion-free' ),
	'menu_parent'     => 'edit.php?post_type=sp_easy_accordion',
	'menu_type'       => 'submenu', // menu, submenu, options, theme, etc.
	'menu_slug'       => 'eap_settings',
	'ajax_save'       => true,
	'show_reset_all'  => false,
	'framework_title' => __( 'Easy Accordion', 'easy-accordion-free' ),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// ----------------------------------------
// a option section for options overview  -
// ----------------------------------------
$options[] = array(
	'name'   => 'general_settings',
	'title'  => __( 'Advanced Settings', 'easy-accordion-free' ),
	'icon'   => 'fa fa-cogs',

	// Begin fields.
	'fields' => array(
		array(
			'id'      => 'eap_data_remove',
			'type'    => 'checkbox',
			'title'   => __( 'Remove Data when Uninstall?', 'easy-accordion-pro' ),
			'desc'    => __( 'Check this box if you would like Easy Accordion Pro to completely remove all of its data when the plugin is deleted.', 'easy-accordion-pro' ),
			'default' => false,
		),
		array(
			'type'    => 'subheading',
			'content' => __( 'Enqueue or Dequeue CSS', 'easy-accordion-free' ),
		),
		array(
			'id'      => 'eap_dequeue_fa_css',
			'type'    => 'switcher',
			'title'   => __( 'Font Awesome CSS', 'easy-accordion-free' ),
			'desc'    => __( 'On/off the switch to enqueue/dequeue font awesome CSS.', 'easy-accordion-free' ),
			'default' => true,
		),
	), // End fields.
);

// ------------------------------
// Custom CSS                   -
// ------------------------------
$options[] = array(
	'name'   => 'custom_css_section',
	'title'  => __( 'Custom CSS', 'easy-accordion-free' ),
	'icon'   => 'fa fa-css3',
	'fields' => array(

		array(
			'id'    => 'ea_custom_css',
			'type'  => 'textarea',
			'title' => __( 'Custom CSS', 'easy-accordion-free' ),
			'desc'  => __( 'Type your custom CSS.', 'easy-accordion-free' ),
		),

	),
);


SP_EAP_Framework::instance( $settings, $options );
