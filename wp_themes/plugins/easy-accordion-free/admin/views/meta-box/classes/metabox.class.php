<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
/**
 * MetaBox Class
 *
 * @since 1.0.0
 * @version 1.0.0
 */
class SP_EAP_Framework_Metabox extends SP_EAP_Framework_Abstract {

	/**
	 * MetaBox options
	 *
	 * @access public
	 * @var array
	 */
	public $options = array();

	/**
	 * Class instance.
	 *
	 * @access private
	 * @var class
	 */
	private static $instance = null;

	/**
	 * Run MetaBox Construct.
	 *
	 * @param mixed $options All the MetaBox options.
	 */
	public function __construct( $options ) {

		$this->options = apply_filters( 'sp_metabox_options', $options );

		if ( ! empty( $this->options ) ) {
			$this->addAction( 'add_meta_boxes', 'add_meta_box' );
			$this->addAction( 'save_post', 'save_post', 10, 2 );
		}

	}

	/**
	 * MetaBox instance.
	 *
	 * @param array $options Options array.
	 * @return statement
	 */
	public static function instance( $options = array() ) {
		if ( is_null( self::$instance ) && SP_EAP_F_ACTIVE_METABOX ) {
			self::$instance = new self( $options );
		}

		return self::$instance;
	}

	/**
	 * Add metabox function.
	 *
	 * @param sting $post_type Add metabox to the post type.
	 * @return void
	 */
	public function add_meta_box( $post_type ) {

		foreach ( $this->options as $value ) {
			add_meta_box(
				$value['id'], $value['title'], array(
					&$this,
					'render_meta_box_content',
				), $value['post_type'], $value['context'], $value['priority'], $value
			);
		}

	}

	/**
	 * Metabox render content
	 *
	 * @param mixed $post The post type.
	 * @param mixed $callback The metabox callback.
	 * @return void
	 */
	public function render_meta_box_content( $post, $callback ) {

		global $post, $sp_errors;

		wp_nonce_field( 'sp-framework-metabox', 'sp-framework-metabox-nonce' );

		$unique       = $callback['args']['id'];
		$sections     = $callback['args']['sections'];
		$meta_value   = get_post_meta( $post->ID, $unique, true );
		$transient    = get_transient( 'eapro-metabox-transient' );
		$sp_errors    = $transient['errors'];
		$has_nav      = ( count( $sections ) >= 2 && $callback['args']['context'] != 'side' ) ? true : false;
		$show_all     = ( ! $has_nav ) ? ' sp-show-all' : '';
		$section_name = ( ! empty( $sections[0]['fields'] ) ) ? $sections[0]['name'] : $sections[1]['name'];
		$section_id   = ( ! empty( $transient['ids'][ $unique ] ) ) ? $transient['ids'][ $unique ] : $section_name;
		$section_id   = sp_get_var( 'sp-section', $section_id );

		echo '<div class="sp-eap-framework sp-metabox-framework">';

		echo '<input type="hidden" name="sp_section_id[' . $unique . ']" class="sp-reset" value="' . $section_id . '">';

		// SP_EAP_Framework.
		$current_screen        = get_current_screen();
		$the_current_post_type = $current_screen->post_type;
		if ( $the_current_post_type == 'sp_easy_accordion' ) {
			?>
			<div class="sp-eap-banner">
				<div class="sp-eap-logo"><img src="<?php echo esc_url( SP_EA_URL . 'admin/img/eap-logo.png' ); ?>" alt="Easy Accordion"></div>
				<div class="sp-eap-short-links">
					<a href="https://shapedplugin.com/support/" target="_blank" title="Ask here to get instant support"><i class="fa fa-life-ring"></i>Support</a>
				</div>
			</div>
			<?php
		} // End SP_EAP_Framework.

		echo '<div class="sp-body' . $show_all . '">';
		if ( $has_nav ) {
			echo '<div class="sp-nav">';
			echo '<ul>';
			foreach ( $sections as $value ) {
				$tab_icon = ( ! empty( $value['icon'] ) ) ? '<i class="sp-icon ' . $value['icon'] . '"></i>' : '';
				if ( isset( $value['fields'] ) ) {
					$active_section = ( $section_id == $value['name'] ) ? ' class="sp-section-active"' : '';
					echo '<li><a href="#" ' . $active_section . ' data-section="' . $value['name'] . '" class="tab-' . $value['name'] . '">' . $tab_icon . $value['title'] . '</a></li>';
				} else {
					echo '<li><div class="sp-separator">' . $tab_icon . $value['title'] . '</div></li>';
				}
			}
			echo '</ul>';

			echo '</div>';
		}

		echo '<div class="sp-content">';

		echo '<div class="sp-sections">';
		foreach ( $sections as $val ) {

			if ( isset( $val['fields'] ) ) {

				$active_content = ( $section_id == $val['name'] ) ? ' style="display: block;"' : '';

				echo '<div id="sp-tab-' . $val['name'] . '" class="sp-section"' . $active_content . '>';
				echo ( isset( $val['title'] ) ) ? '<div class="sp-section-title"><h3>' . $val['title'] . '</h3></div>' : '';

				foreach ( $val['fields'] as $field_key => $field ) {

					$default    = ( isset( $field['default'] ) ) ? $field['default'] : '';
					$elem_id    = ( isset( $field['id'] ) ) ? $field['id'] : '';
					$elem_value = ( is_array( $meta_value ) && isset( $meta_value[ $elem_id ] ) ) ? $meta_value[ $elem_id ] : $default;
					echo sp_eap_add_element( $field, $elem_value, $unique );

				}
				echo '</div>';
			}
		}
		echo '</div>';

		echo '<div class="clear"></div>';

		echo '</div>';

		echo ( $has_nav ) ? '<div class="sp-nav-background"></div>' : '';

		echo '<div class="clear"></div>';

		echo '</div>'; // End sp-body.

		?>

		<div class="eapro_shortcode_divider"></div>
		<div class="eapro_shortcode text-center">
				<div class="eapro-col-lg-3">
					<div class="eapro_shortcode_content">
						<span class="eapro-shortcode-title"><?php _e( 'Shortcode', 'easy-accordion-free' ); ?> </span>
						<div class="eapro-sc-code selectable" >[sp_easyaccordion 
						<?php echo 'id="' . $post->ID . '"'; ?>]
						</div>
					</div>
				</div>
				<div class="eapro-col-lg-6">
					<div class="eapro_shortcode_content">
						<span class="eapro-shortcode-title">
						<?php
						_e( 'Template Include', 'easy-accordion-free' );
						?>
						</span>
						<div class="eapro-sc-code selectable">
							&lt;?php echo do_shortcode( '[sp_easyaccordion id="<?php echo $post->ID; ?>"]' );
							?&gt;</div>
					</div>
				</div>
		
			<div class="sp-ea-upgrade-area">
					<div class="sp-ea-upgrade-header text-center">
							<h1>Get more Advanced Functionality & Flexibility with Pro!</h1>
							<p>Upgrade to Pro Version of  Easy Accordion to benefit from all features!</p>
							<div class="sp-ea-button">
								<div>
									<a href="https://shapedplugin.com/plugin/easy-accordion-pro/" class="btn btn-one" target="_blank" title="">Upgrade to Pro!</a>
								</div>
								<div>
									<a href="https://shapedplugin.com/demo/easy-accordion-pro/" class="btn btn-two" target="_blank" title="">All Features & Demo</a>
								</div>
							</div>
							<img src="<?php echo SP_EA_URL . 'admin/views/meta-box/assets/images/aa-img.jpg'; ?>" alt="">
					</div>
			
				<div class="sp-ea-upgrade-body">
					<h1 class="text-center">Powerful Premium Features Include</h2>
					<p class="text-center">You will have access to tons of premium features which help you to create professional looking <br>
					accordions easily. Some of the remarkable features:</p>
					<div class="upgrade-featrue container">

					
				<div class="col-6">
					<ul>
						<li>16+ Beautiful Themes with Preview.</li>
						<li>2 Layouts. (Horizontal and Vertical)</li>
						<li>Advanced Shortcode Generator.</li>
						<li>Multi-level or Nested Accordion.</li>
						<li>14+ Expand & Collapse Icon Style Sets.</li>
						<li> Accordion from Post & Category.</li>
						<li>Accordion from Custom Post Types & Taxonomy.</li>
						<li>Group Accordion FAQs Showcase.</li>
						<li>Limit To Display Number of Accordion.</li>
						<li>25+ Smooth Animation & Effects.</li>
						<li> Margin Between Accordions.</li>
						<li>Accordion Border and Radius options.</li>
					</ul>
				</div>
				<div class="col-6">
					<ul>
						<li> 840+ Google Fonts. (Typography Options)</li>
						<li>Supported any Contents. (e.g. HTML, shortcodes, images, YouTube, audio etc.)</li>
						<li>Accordion Title Background Color & Custom Padding.</li>
						<li> Accordion Description Background Color.</li>
						<li>Accordion Description Custom Padding.</li>
						<li>FontAwesome Icon Picker before Accordion Title.</li>
						<li>Multilingual & RTL Ready.</li>
						<li>Widget Supported.</li>
						<li>Multi-site Supported.</li>
						<li>Developer friendly & easy to customize.</li>
						<li>And much more options.</li>
					</ul>
				</div>
					</div>
				</div>
				<div class="sp-ea-upgrade-footer text-center">
					<h1>Join the 3000+ Happy Users</h2>
					<div class="buy-btn">
						<a href="https://shapedplugin.com/plugin/easy-accordion-pro/" target="_blank">Get a License Now!</a>
					</div>
				</div>
			</div>
				</div>
			<?php
			echo '</div>';
	}

	/**
	 * Save metabox options.
	 *
	 * @param integer $post_id The post ID.
	 * @param string  $post The Post.
	 * @return void
	 */
	public function save_post( $post_id, $post ) {

		if ( wp_verify_nonce( sp_get_var( 'sp-framework-metabox-nonce' ), 'sp-framework-metabox' ) ) {

			$errors    = array();
			$post_type = sp_get_var( 'post_type' );

			foreach ( $this->options as $request_value ) {

				if ( in_array( $post_type, (array) $request_value['post_type'] ) ) {

					$request_key = $request_value['id'];
					$request     = sp_get_var( $request_key, array() );

					// ignore _nonce.
					if ( isset( $request['_nonce'] ) ) {
						unset( $request['_nonce'] );
					}

					foreach ( $request_value['sections'] as $key => $section ) {

						if ( isset( $section['fields'] ) ) {

							foreach ( $section['fields'] as $field ) {

								if ( isset( $field['type'] ) && isset( $field['id'] ) ) {

									$field_value = sp_get_vars( $request_key, $field['id'] );

									// sanitize options.
									if ( isset( $field['sanitize'] ) && $field['sanitize'] !== false ) {
										$sanitize_type = $field['sanitize'];
									} elseif ( ! isset( $field['sanitize'] ) ) {
										$sanitize_type = $field['type'];
									}

									if ( has_filter( 'sp_sanitize_' . $sanitize_type ) ) {
										$request[ $field['id'] ] = apply_filters( 'sp_sanitize_' . $sanitize_type, $field_value, $field, $section['fields'] );
									}

									// validate options.
									if ( isset( $field['validate'] ) && has_filter( 'sp_validate_' . $field['validate'] ) ) {

										$validate = apply_filters( 'sp_validate_' . $field['validate'], $field_value, $field, $section['fields'] );

										if ( ! empty( $validate ) ) {

											$meta_value = get_post_meta( $post_id, $request_key, true );

											$errors[ $field['id'] ]  = array(
												'code'    => $field['id'],
												'message' => $validate,
												'type'    => 'error',
											);
											$default_value           = isset( $field['default'] ) ? $field['default'] : '';
											$request[ $field['id'] ] = ( isset( $meta_value[ $field['id'] ] ) ) ? $meta_value[ $field['id'] ] : $default_value;
										}
									}
								}
							}
						}
					}

					$request = apply_filters( 'sp_save_post', $request, $request_key, $post );

					if ( empty( $request ) ) {

						delete_post_meta( $post_id, $request_key );

					} else {

						if ( get_post_meta( $post_id, $request_key ) ) {

							update_post_meta( $post_id, $request_key, $request );

						} else {
							add_post_meta( $post_id, $request_key, $request );
						}
					}

					$transient['ids'][ $request_key ] = sp_get_vars( 'sp_section_id', $request_key );
					$transient['errors']              = $errors;

				}
			}
			set_transient( 'eapro-metabox-transient', $transient, 10 );

		}

	}

}
