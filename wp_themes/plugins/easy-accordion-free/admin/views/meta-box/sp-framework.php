<?php
/**
 * The main file for the SP meta-box framework.
 *
 * @package WP_accordion_Pro
 * @subpackage WP_accordion_Pro/admin/views/meta-box
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.

// ------------------------------------------------------------------------------------------------
require_once plugin_dir_path( __FILE__ ) . '/sp-framework-path.php';
// ------------------------------------------------------------------------------------------------
if ( ! function_exists( 'sp_eap_framework_init' ) && ! class_exists( 'SP_EAP_Framework' ) ) {

	/**
	 * SP meta box framework for ShapedPlugin
	 *
	 * @since 3.0.0
	 * @return void
	 */
	function sp_eap_framework_init() {

		// Active modules.
		defined( 'SP_EAP_F_ACTIVE_METABOX' ) || define( 'SP_EAP_F_ACTIVE_METABOX', true );
		defined( 'SP_EAP_F_ACTIVE_FRAMEWORK' ) || define( 'SP_EAP_F_ACTIVE_FRAMEWORK', true );

		// Helpers.
		sp_eap_locate_template( 'functions/fallback.php' );
		sp_eap_locate_template( 'functions/helpers.php' );
		sp_eap_locate_template( 'functions/actions.php' );
		sp_eap_locate_template( 'functions/enqueue.php' );
		sp_eap_locate_template( 'functions/sanitize.php' );
		sp_eap_locate_template( 'functions/validate.php' );

		// Classes.
		sp_eap_locate_template( 'classes/abstract.class.php' );
		sp_eap_locate_template( 'classes/options.class.php' );
		sp_eap_locate_template( 'classes/metabox.class.php' );
		sp_eap_locate_template( 'classes/framework.class.php' );

		// Configs.
		sp_eap_locate_template( 'config/metabox.config.php' );
		sp_eap_locate_template( 'config/framework.config.php' );

	}
	add_action( 'init', 'sp_eap_framework_init', 10 );
}
