<?php if ( ! defined( 'ABSPATH' ) ) {
	die; } // Cannot access pages directly.
/**
 *
 * Field: accordion_type
 *
 * @since 1.0.0
 * @version 1.0.0
 */
class SP_EAP_Framework_Option_accordion_type extends SP_EAP_Framework_Options {

	/**
	 * Carousel type constructor.
	 *
	 * @param array  $field The field array.
	 * @param string $value The value of the accordion type.
	 * @param string $unique The id for the accordion type field.
	 */
	public function __construct( $field, $value = '', $unique = '' ) {
		parent::__construct( $field, $value, $unique );
	}

	/**
	 * Function for the button set field.
	 *
	 * @return void
	 */
	public function output() {

		$input_type = ( ! empty( $this->field['radio'] ) ) ? 'radio' : 'checkbox';
		$input_attr = ( ! empty( $this->field['multi_select'] ) ) ? '[]' : '';

		echo $this->element_before();
		echo ( empty( $input_attr ) ) ? '<div class="sp-field-accordion-type">' : '';

		if ( isset( $this->field['options'] ) ) {
			$options = $this->field['options'];
			foreach ( $options as $key => $value ) {
				$pro_only = ( true == $value['pro_only'] ) ? 'disabled' : '';
				echo '<label><input ' . $pro_only . ' type="' . $input_type . '" name="' . $this->element_name( $input_attr ) . '" value="' . $key . '"' . $this->element_class() . $this->element_attributes( $key ) . $this->checked( $this->element_value(), $key ) . '/><span><p class="sp-accordion-type">' . $value['text'] . '</p></span></label>';
			}
		}
		echo ( empty( $input_attr ) ) ? '</div>' : '';
		echo $this->element_after();

	}
}

