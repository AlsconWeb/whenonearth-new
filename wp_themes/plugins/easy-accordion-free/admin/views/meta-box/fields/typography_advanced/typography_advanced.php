<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.

/**
 *
 * Field: Typography Advanced
 *
 * @since 1.0.0
 * @version 1.0.0
 */
class SP_EAP_Framework_Option_typography_advanced extends SP_EAP_Framework_Options {

	public function __construct( $field, $value = '', $unique = '' ) {
		parent::__construct( $field, $value, $unique );
	}

	public function output() {

		echo $this->element_before();

		$defaults_value = array(
			'family'      => 'Arial',
			'variant'     => 'regular',
			'font'        => 'websafe',
			'size'        => '14',
			'height'      => '',
			'alignment'   => '',
			'transform'   => '',
			'spacing'     => '',
			'color'       => '',
			'bg_color'    => '',
			'hover_color' => '',
		);

		$default_variants = apply_filters(
			'sp_websafe_fonts_variants', array(
				'regular',
				'italic',
				'700',
				'700italic',
				'inherit',
			)
		);

		$websafe_fonts = apply_filters(
			'sp_websafe_fonts', array(
				'Arial',
				'Arial Black',
				'Comic Sans MS',
				'Impact',
				'Lucida Sans Unicode',
				'Tahoma',
				'Trebuchet MS',
				'Verdana',
				'Courier New',
				'Lucida Console',
				'Georgia, serif',
				'Palatino Linotype',
				'Times New Roman',
			)
		);

		$value         = wp_parse_args( $this->element_value(), $defaults_value );
		$family_value  = $value['family'];
		$variant_value = $value['variant'];
		$is_variant    = ( isset( $this->field['variant'] ) && $this->field['variant'] === false ) ? false : true;
		$google_json   = sp_eap_get_google_fonts();

		// Container
		echo '<div class="sp_eap_font_field" data-id="' . $this->field['id'] . '">';

		if ( is_object( $google_json ) ) {

			$googlefonts = array();

			foreach ( $google_json->items as $key => $font ) {
				$googlefonts[ $font->family ] = $font->variants;
			}

			$is_google = ( array_key_exists( $family_value, $googlefonts ) ) ? true : false;

			echo '<div class="sp-element sp-typography-family sp-eap-select-wrapper">Font Family<br>';
			echo '<select disabled name="' . $this->element_name( '[family]' ) . '" class="sp-eap-select-css sp-typo-family" data-atts="family"' . $this->element_attributes() . '>';

			do_action( 'sp_typography_family', $family_value, $this );

			echo '<optgroup label="' . __( 'Web Safe Fonts', 'easy-accordion-free' ) . '">';
			foreach ( $websafe_fonts as $websafe_value ) {
				echo '<option disabled value="' . $websafe_value . '" data-variants="' . implode( '|', $default_variants ) . '" data-type="websafe"' . selected( $websafe_value, $family_value, true ) . '>' . $websafe_value . '</option>';
			}
			echo '</optgroup>';

			echo '<optgroup label="' . __( 'Google Fonts', 'easy-accordion-free' ) . '">';
			foreach ( $googlefonts as $google_key => $google_value ) {

				echo '<option disabled value="' . $google_key . '" data-variants="' . implode( '|', $google_value ) . '" data-type="google" ' . selected( $google_key, $family_value, true ) . '>' . $google_key . '</option>';
			}
			echo '</optgroup>';

			echo '</select>';
			echo '</div>';

			if ( ! empty( $is_variant ) ) {

				$variants = ( $is_google ) ? $googlefonts[ $family_value ] : $default_variants;
				$variants = ( $value['font'] === 'google' || $value['font'] === 'websafe' ) ? $variants : 'regular';
				echo '<div class="sp-element sp-typography-variant sp-eap-select-wrapper">Font Weight<br>';
				echo '<select disabled  name="' . $this->element_name( '[variant]' ) . '" class="sp-eap-select-css sp-typo-variant" data-atts="variant">';
				foreach ( $variants as $variant ) {
					echo '<option value="' . $variant . '" ' . $this->checked( $variant_value, $variant, 'selected' ) . '>' . $variant . '</option>';
				}
				echo '</select>';
				echo '</div>';

			}

			echo sp_eap_add_element(
				array(
					'pseudo'     => true,
					'type'       => 'number',
					'name'       => $this->element_name( '[size]' ),
					'value'      => $value['size'],
					'pro_only'   => true,
					'default'    => ( isset( $this->field['default']['size'] ) ) ? $this->field['default']['size'] : '',
					'wrap_class' => 'small-input sp-font-size',
					'before'     => 'Font Size<br>',
					'attributes' => array(
						'title' => __( 'Font Size', 'easy-accordion-free' ),
					),
				)
			);

			echo sp_eap_add_element(
				array(
					'pseudo'     => true,
					'type'       => 'number',
					'name'       => $this->element_name( '[height]' ),
					'value'      => $value['height'],
					'default'    => ( isset( $this->field['default']['height'] ) ) ? $this->field['default']['height'] : '',
					'wrap_class' => 'small-input sp-font-height',
					'pro_only'   => true,
					'before'     => 'Line Height<br>',
					'attributes' => array(
						'title' => __( 'Line Height', 'easy-accordion-free' ),
					),
				)
			);
			echo '<div class="sp-divider"></div>';
			echo sp_eap_add_element(
				array(
					'pseudo'     => true,
					'type'       => 'select_typo',
					'name'       => $this->element_name( '[alignment]' ),
					'value'      => $value['alignment'],
					'pro_only'   => true,
					'default'    => ( isset( $this->field['default']['alignment'] ) ) ? $this->field['default']['alignment'] : '',
					'wrap_class' => 'small-input sp-font-alignment sp-eap-select-wrapper',
					'class'      => 'sp-eap-select-css',
					'before'     => 'Alignment<br>',
					'options'    => array(
						'inherit' => __( 'Inherit', 'easy-accordion-free' ),
						'left'    => __( 'Left', 'easy-accordion-free' ),
						'center'  => __( 'Center', 'easy-accordion-free' ),
						'right'   => __( 'Right', 'easy-accordion-free' ),
						'justify' => __( 'Justify', 'easy-accordion-free' ),
						'initial' => __( 'Initial', 'easy-accordion-free' ),
					),

				)
			);
			echo sp_eap_add_element(
				array(
					'pseudo'     => true,
					'type'       => 'select_typo',
					'name'       => $this->element_name( '[transform]' ),
					'value'      => $value['transform'],
					'pro_only'   => true,
					'default'    => ( isset( $this->field['default']['transform'] ) ) ? $this->field['default']['transform'] : '',
					'wrap_class' => 'small-input sp-font-transform sp-eap-select-wrapper',
					'class'      => 'sp-eap-select-css',
					'before'     => 'Transform<br>',
					'options'    => array(
						'none'       => __( 'None', 'easy-accordion-free' ),
						'capitalize' => __( 'Capitalize', 'easy-accordion-free' ),
						'uppercase'  => __( 'Uppercase', 'easy-accordion-free' ),
						'lowercase'  => __( 'Lowercase', 'easy-accordion-free' ),
						'initial'    => __( 'Initial', 'easy-accordion-free' ),
						'inherit'    => __( 'Inherit', 'easy-accordion-free' ),
					),
				)
			);
			echo sp_eap_add_element(
				array(
					'pseudo'     => true,
					'type'       => 'select_typo',
					'name'       => $this->element_name( '[spacing]' ),
					'value'      => $value['spacing'],
					'pro_only'   => true,
					'default'    => ( isset( $this->field['default']['spacing'] ) ) ? $this->field['default']['spacing'] : '',
					'wrap_class' => 'small-input sp-font-spacing sp-eap-select-wrapper',
					'class'      => 'sp-eap-select-css',
					'before'     => 'Letter Spacing<br>',
					'options'    => array(
						'normal' => __( 'Normal', 'easy-accordion-free' ),
						'.3px'   => __( '0.3px', 'easy-accordion-free' ),
						'.5px'   => __( '0.5px', 'easy-accordion-free' ),
						'1px'    => __( '1px', 'easy-accordion-free' ),
						'1.5px'  => __( '1.5px', 'easy-accordion-free' ),
						'2px'    => __( '2px', 'easy-accordion-free' ),
						'3px'    => __( '3px', 'easy-accordion-free' ),
						'5px'    => __( '5px', 'easy-accordion-free' ),
						'10px'   => __( '10px', 'easy-accordion-free' ),
					),
				)
			);
			echo '<div class="sp-divider"></div>';
			if ( isset( $this->field['color'] ) && $this->field['color'] == true ) {
				echo '<div class="sp-element sp-typography-color">' . __( 'Color', 'easy-accordion-free' ) . '<br>';
				echo sp_eap_add_element(
					array(
						'pseudo'     => true,
						'id'         => $this->field['id'] . '_color',
						'type'       => 'color_picker',
						'pro_only'   => true,
						'name'       => $this->element_name( '[color]' ),
						'attributes' => array(
							'data-atts' => 'bgcolor',
						),
						'value'      => $value['color'],
						'default'    => ( isset( $this->field['default']['color'] ) ) ? $this->field['default']['color'] : '',
						'rgba'       => ( isset( $this->field['rgba'] ) && $this->field['rgba'] === false ) ? false : '',
					)
				);
				echo '</div>';
			}
			if ( isset( $this->field['hover_color'] ) && $this->field['hover_color'] == true ) {
				echo '<div class="sp-element sp-typography-hover-color">' . __( 'Hover Color', 'easy-accordion-free' ) . '<br>';
				echo sp_eap_add_element(
					array(
						'pseudo'     => true,
						'id'         => $this->field['id'] . '_hover_color',
						'type'       => 'color_picker',
						'pro_only'   => true,
						'name'       => $this->element_name( '[hover_color]' ),
						'attributes' => array(
							'data-atts' => 'hovercolor',
						),
						'value'      => $value['hover_color'],
						'default'    => ( isset( $this->field['default']['hover_color'] ) ) ? $this->field['default']['hover_color'] : '',
						'rgba'       => ( isset( $this->field['rgba'] ) && $this->field['rgba'] === false ) ? false : '',
					)
				);
				echo '</div>';
			}
			if ( isset( $this->field['bg_color'] ) && $this->field['bg_color'] == true ) {
				echo '<div class="sp-element sp-typography-bg-color">' . __( 'Background Color', 'easy-accordion-free' ) .
					 '<br>';
				echo sp_eap_add_element(
					array(
						'pseudo'     => true,
						'id'         => $this->field['id'] . '_bg_color',
						'type'       => 'color_picker',
						'pro_only'   => true,
						'name'       => $this->element_name( '[bg_color]' ),
						'attributes' => array(
							'data-atts' => 'bgcolor',
						),
						'value'      => $value['bg_color'],
						'default'    => ( isset( $this->field['default']['bg_color'] ) ) ? $this->field['default']['bg_color'] : '',
						'rgba'       => ( isset( $this->field['rgba'] ) && $this->field['rgba'] === false ) ? false : '',
					)
				);
				echo '</div>';
			}

			/**
			 * Font Preview
			 */
			if ( isset( $this->field['preview'] ) && $this->field['preview'] == true ) {
				if ( isset( $this->field['preview_text'] ) ) {
					$preview_text = $this->field['preview_text'];
				} else {
					$preview_text = 'Lorem ipsum dolor sit amet, pro ad sanctus admodum, vim at insolens appellantur. Eum veri adipiscing an, probo nonumy an vis.';
				}
				echo '<div id="preview-' . $this->field['id'] . '" class="sp-font-preview">' . $preview_text . '</div>';
			}

			echo '<input type="text" name="' . $this->element_name( '[font]' ) . '" class="sp-typo-font hidden" data-atts="font" value="' . $value['font'] . '" />';

		} else {

			echo __( 'Error! Can not load json file.', 'easy-accordion-free' );

		}

		// end container
		echo '</div>';

		echo $this->element_after();

	}

}
