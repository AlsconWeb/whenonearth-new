=== HTTP/2 Push, Async JavaScript, Defer Render Blocking CSS, HTTP2 server push ===
Contributors: rajeshsingh520
Donate link: piwebsolution.com
Tags: http2 server push, Async CSS, Defer CSS, Defer JS, Async JS
Requires at least: 4.0
Tested up to: 5.2
License: GPLv2 or later
Requires PHP: 5.4
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Stable tag: trunk

Push pre-load any resource, Async JavaScript, Defer Render Blocking CSS, with fine rule set to control js and css on different page types, HTTP2 Server push

== Description ==

* Push / Pre-load all JS files in site with one simple option
* Push / Pre-load all the CSS files in your website
* Push / Pre-load other resources throughout the site or based on the page types
* Load CSS Asynchronous or Remove any CSS file throughout the site, or there is a conditional selector that you can apply
* Async / Defer / Remove any JS file throughout the site or based on the WordPress page type

Apart from this it also offer ability to remove Css and JS file from specific pages based in the selected page tag conditions 

Eg: if css path is https://s.w.org/style/wp4.css

then you can match it with wp4.css or style/wp4.css or s.w.org/style/wp4.css

you use 2nd method (style/wp4.css) for more precise selection (this avoid error when there are 2 style with same file name)
