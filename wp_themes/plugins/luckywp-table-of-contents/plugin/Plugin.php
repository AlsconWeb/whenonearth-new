<?php

namespace luckywp\tableOfContents\plugin;

use luckywp\tableOfContents\admin\Admin;
use luckywp\tableOfContents\admin\Rate;
use luckywp\tableOfContents\core\base\BasePlugin;
use luckywp\tableOfContents\core\base\Request;
use luckywp\tableOfContents\core\base\View;
use luckywp\tableOfContents\core\helpers\ArrayHelper;
use luckywp\tableOfContents\core\wp\Options;
use luckywp\tableOfContents\front\Front;
use luckywp\tableOfContents\plugin\editorBlock\EditorBlock;
use luckywp\tableOfContents\plugin\mcePlugin\McePlugin;
use WP_Post_Type;

/**
 * @property Admin $admin
 * @property Front $front
 * @property Request $request
 * @property Settings $settings
 * @property EditorBlock $editorBlock
 * @property McePlugin $mcePlugin
 * @property Options $options
 * @property Rate $rate
 * @property Shortcode $shortcode
 * @property View $view
 *
 * @property WP_Post_Type[] $postTypes
 * @property array $depthsList
 * @property array $numerationsList
 * @property array $positionsList
 * @property array $blockSizeUnitsList
 * @property array $fontSizeUnitsList
 * @property array $fontWeightsList
 * @property array $floatsList
 * @property array $colorSchemesList
 */
class Plugin extends BasePlugin
{

    /**
     * @var bool
     */
    public $isTheContent = false;

    /**
     * Инициализация
     */
    public function init()
    {
        add_filter('the_content', function ($content) {
            $this->isTheContent = true;
            return $content;
        }, 1);
        add_filter('the_content', function ($content) {
            $this->isTheContent = false;
            return $content;
        }, 10000);
        add_action('widgets_init', function () {
            register_widget(WpWidget::class);
        });
    }

    private $_postTypes;

    /**
     * @return WP_Post_Type[]
     */
    public function getPostTypes()
    {
        if ($this->_postTypes === null) {
            $this->_postTypes = get_post_types([
                'public' => true,
            ], 'objects');
        }
        return $this->_postTypes;
    }

    /**
     * @return array
     */
    public function getDepthsList()
    {
        return [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
        ];
    }

    /**
     * @return array
     */
    public function getNumerationsList()
    {
        return [
            'none' => esc_html__('Without numeration', 'lwptoc'),
            'decimalnested' => esc_html__('Decimal numbers (nested)', 'lwptoc'),
            'decimal' => esc_html__('Decimal numbers', 'lwptoc'),
            'romannested' => esc_html__('Roman numbers (nested)', 'lwptoc'),
            'roman' => esc_html__('Roman numbers', 'lwptoc'),
        ];
    }

    /**
     * @return array
     */
    public function getPositionsList()
    {
        return [
            'beforefirstheading' => esc_html__('Before first heading', 'lwptoc'),
            'afterfirstheading' => esc_html__('After first heading', 'lwptoc'),
            'afterfirstblock' => esc_html__('After first block (paragraph, list or heading)', 'lwptoc'),
            'top' => esc_html__('Top', 'lwptoc'),
            'bottom' => esc_html__('Bottom', 'lwptoc'),
        ];
    }

    /**
     * @return array
     */
    public function getBlockSizeUnitsList()
    {
        return [
            'px' => 'px',
            '%' => '%',
        ];
    }

    /**
     * @return array
     */
    public function getFontSizeUnitsList()
    {
        return [
            '%' => '%',
            'em' => 'em',
            'pt' => 'pt',
            'px' => 'px',
        ];
    }

    /**
     * @return array
     */
    public function getFontWeightsList()
    {
        return [
            'thin' => esc_html__('Thin', 'lwptoc'),
            'extralight' => esc_html__('Extra Light', 'lwptoc'),
            'light' => esc_html__('Light', 'lwptoc'),
            'normal' => esc_html__('Normal', 'lwptoc'),
            'medium' => esc_html__('Medium', 'lwptoc'),
            'semibold' => esc_html__('Semi Bold', 'lwptoc'),
            'bold' => esc_html__('Bold', 'lwptoc'),
            'extrabold' => esc_html__('Extra Bold', 'lwptoc'),
            'heavy' => esc_html__('Heavy', 'lwptoc'),
        ];
    }

    /**
     * @param string $id
     * @return string|null
     */
    public function fontWeightToValue($id)
    {
        return ArrayHelper::getValue([
            'thin' => '100',
            'extralight' => '200',
            'light' => '300',
            'normal' => 'normal',
            'medium' => '500',
            'semibold' => '600',
            'bold' => 'bold',
            'extrabold' => '800',
            'heavy' => '900',
        ], $id);
    }

    /**
     * @return array
     */
    public function getFloatsList()
    {
        return [
            'none' => esc_html__('None', 'lwptoc'),
            'left' => esc_html__('Left', 'lwptoc'),
            'right' => esc_html__('Right', 'lwptoc'),
            'rightwithoutflow' => esc_html__('Right without flow', 'lwptoc'),
            'center' => esc_html__('Center', 'lwptoc'),
        ];
    }

    /**
     * @return array
     */
    public function getColorSchemesList()
    {
        return [
            'light' => esc_html__('Light Colors', 'lwptoc'),
            'dark' => esc_html__('Dark Colors', 'lwptoc'),
            'white' => esc_html__('White', 'lwptoc'),
            'transparent' => esc_html__('Transparent', 'lwptoc'),
        ];
    }

    /**
     * @return array
     */
    public function getHashFormatsList()
    {
        return [
            'asheading' => esc_html__('As heading (#Example_Heading_Text)', 'lwptoc'),
            'counter' => esc_html__('Counter (#lpwtoc1, #lwptoc2, …)', 'lwptoc'),
        ];
    }

    /**
     * @param bool $withCustom
     * @return array
     */
    public function getWidthsList($withCustom = true)
    {
        $widths = [
            'auto' => esc_html__('Auto', 'lwptoc'),
            'full' => esc_html__('Full Width', 'lwptoc'),
            'custom' => esc_html__('Custom Value', 'lwptoc'),
        ];
        if (!$withCustom) {
            unset($widths['custom']);
        }
        return $widths;
    }

    /**
     * @param string $width
     * @return bool
     */
    public function isCustomWidth($width)
    {
        return !array_key_exists($width, $this->getWidthsList(false));
    }

    /**
     * @param string $width
     * @return string
     */
    public function widthToLabel($width)
    {
        if ($this->isCustomWidth($width)) {
            return $width;
        }
        return $this->getWidthsList()[$width];
    }

    /**
     * @param string $fontSize
     * @return string
     */
    public function fontSizeToLabel($fontSize)
    {
        return $fontSize == 'default' ? esc_html__('Default', 'lwptoc') : $fontSize;
    }

    /**
     * @return array
     */
    public function getHeadingsList()
    {
        return [
            'h1' => 'H1',
            'h2' => 'H2',
            'h3' => 'H3',
            'h4' => 'H4',
            'h5' => 'H5',
            'h6' => 'H6',
        ];
    }

    /**
     * @param string|array $value
     * @return array
     */
    public function skipHeadingLevelToArray($value)
    {
        if (is_array($value)) {
            $ids = $value;
        } else {
            $ids = explode(',', (string)$value);
            $ids = array_map('trim', $ids);
            $ids = array_map('strtolower', $ids);
        }
        $list = $this->getHeadingsList();
        return array_filter($ids, function ($id) use ($list) {
            return array_key_exists($id, $list);
        });
    }

    /**
     * @param string $value
     * @return string
     */
    public function skipHeadingLevelToLabel($value)
    {
        $ids = $this->skipHeadingLevelToArray($value);
        $labels = [];
        if ($ids) {
            $list = $this->getHeadingsList();
            foreach ($ids as $id) {
                $labels[] = $list[$id];
            }
        }
        return $labels ? implode(', ', $labels) : esc_html__('None', 'lwptoc');
    }

    /**
     * @param string $string
     * @return array
     */
    public function skipHeadingTextToArray($string)
    {
        $string = str_replace('\|', '%%lwptocplug%%', $string);
        $els = explode('|', $string);
        $els = array_map('trim', $els);
        $els = array_filter($els, function ($el) {
            return $el != '';
        });
        $els = array_map(function ($el) {
            return str_replace('%%lwptocplug%%', '|', $el);
        }, $els);
        return $els;
    }

    /**
     * @param string $string
     * @return string
     */
    public function skipHeadingTextToMultipleString($string)
    {
        return implode(PHP_EOL, $this->skipHeadingTextToArray($string));
    }

    /**
     * @param $string
     * @return string
     */
    public function skipHeadingTextMultipleStringToString($string)
    {
        $els = explode(PHP_EOL, $string);
        $els = array_map('trim', $els);
        $els = array_filter($els, function ($el) {
            return $el != '';
        });
        $els = array_map(function ($el) {
            return str_replace('|', '\|', $el);
        }, $els);
        return implode('|', $els);
    }

    /**
     * @param string|array $src
     * @return string|false
     */
    public function skipHeadingTextToRegex($src)
    {
        if (!is_array($src)) {
            $src = $this->skipHeadingTextToArray($src);
        }
        $regex = [];
        foreach ($src as $t) {
            $t = strtr($t, [
                '\\\\' => '%%lwptocslash%%',
                '\*' => '%%lwptocstar%%',
            ]);
            $t = str_replace('*', '___lwptocany___', $t);
            $t = strtr($t, [
                '%%lwptocslash%%' => '\\',
                '%%lwptocstar%%' => '*',
            ]);
            $t = preg_quote($t);
            $t = str_replace('___lwptocany___', '.*', $t);
            $regex[] = $t;
        }
        return $regex ? '#^(' . implode('|', $regex) . ')$#i' : false;
    }

    private function pluginI18n()
    {
        __('Creates a table of contents for your posts/pages. Works automatically or manually (via shortcode, Gutenberg block or widget).', 'lwptdr');
    }
}
