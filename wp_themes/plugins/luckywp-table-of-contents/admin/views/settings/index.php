<?php

use luckywp\tableOfContents\admin\Rate;
use luckywp\tableOfContents\core\Core;

?>
<div class="wrap">
    <a href="<?= Rate::LINK ?>" target="_blank" class="lwptocSettingsRate"><?= sprintf(
            esc_html__('Leave a %s plugin review on WordPress.org', 'lwptoc'),
            '★★★★★'
        ) ?></a>
    <h1><?= esc_html__('Table of Contents Settings', 'lwptoc') ?></h1>
    <?php Core::$plugin->settings->showPage(false) ?>
</div>