<?php

namespace luckywp\tableOfContents\admin;

use luckywp\tableOfContents\admin\controllers\EditorBlockController;
use luckywp\tableOfContents\admin\controllers\MetaboxController;
use luckywp\tableOfContents\admin\controllers\RateController;
use luckywp\tableOfContents\admin\controllers\SettingsController;
use luckywp\tableOfContents\admin\controllers\ShortcodeController;
use luckywp\tableOfContents\admin\controllers\WidgetController;
use luckywp\tableOfContents\admin\widgets\metabox\Metabox;
use luckywp\tableOfContents\admin\widgets\OverrideColorBadge;
use luckywp\tableOfContents\core\admin\helpers\AdminUrl;
use luckywp\tableOfContents\core\base\BaseObject;
use luckywp\tableOfContents\core\Core;
use luckywp\tableOfContents\core\helpers\ArrayHelper;
use luckywp\tableOfContents\core\helpers\Html;
use luckywp\tableOfContents\plugin\PostSettings;

class Admin extends BaseObject
{

    protected $pageSettingsHook;

    public function init()
    {
        if (is_admin()) {
            add_action('admin_menu', [$this, 'menu']);
            add_action('admin_enqueue_scripts', [$this, 'assets'], 9);
            add_action('add_meta_boxes', [$this, 'addMetaBoxes']);

            // Ссылки в списке плагинов
            add_filter('plugin_action_links_' . Core::$plugin->basename, function ($links) {
                array_unshift($links, Html::a(esc_html__('Settings', 'lwptoc'), AdminUrl::toOptions('settings')));
                return $links;
            });

            // Контроллеры
            MetaboxController::getInstance();
            ShortcodeController::getInstance();
            EditorBlockController::getInstance();
            WidgetController::getInstance();
            RateController::getInstance();
        }
    }

    public function menu()
    {
        $this->pageSettingsHook = add_submenu_page(
            'options-general.php',
            esc_html__('Table of Contents', 'lwptoc'),
            esc_html__('Table of Contents', 'lwptoc'),
            'manage_options',
            Core::$plugin->prefix . 'settings',
            [SettingsController::className(), 'router']
        );
    }

    /**
     * @return array
     */
    public function getMetaboxPostTypes()
    {
        return Core::$plugin->settings->getPostTypesForProcessingHeadings();
    }

    public function addMetaBoxes()
    {
        if (current_user_can('edit_posts') && $this->getMetaboxPostTypes()) {
            add_meta_box(
                Core::$plugin->prefix . '_postSettings',
                esc_html__('Table of Contents', 'lwptoc'),
                function ($post) {
                    echo Metabox::widget([
                        'post' => $post,
                    ]);
                },
                $this->getMetaboxPostTypes(),
                'side',
                'low'
            );
        }
    }

    public function assets($hook)
    {
        global $post;
        if (in_array($hook, [$this->pageSettingsHook, 'post.php', 'post-new.php', 'widgets.php', 'settings_page_lwptoc_settings'])) {
            wp_enqueue_style('wp-color-picker');
            wp_enqueue_script('wp-color-picker');
        }
        wp_enqueue_style(Core::$plugin->prefix . 'adminMain', Core::$plugin->url . '/admin/assets/main.min.css', [], Core::$plugin->version);
        wp_enqueue_script(Core::$plugin->prefix . 'adminMain', Core::$plugin->url . '/admin/assets/main.min.js', ['jquery'], Core::$plugin->version);
        wp_localize_script(Core::$plugin->prefix . 'adminMain', 'lwptocMain', [
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce(Core::$plugin->prefix . 'adminMain'),
            'postId' => in_array($hook, ['post.php', 'post-new.php']) ? ArrayHelper::getValue($post, 'ID') : null,
            'shortcodeTag' => Core::$plugin->shortcode->getTag(),
            'tableOfContents' => esc_html__('Table of Contents', 'lwptoc'),
            'Edit' => esc_html__('Edit', 'lwptoc'),
        ]);
    }

    public function checkAjaxReferer()
    {
        check_ajax_referer(Core::$plugin->prefix . 'adminMain');
    }

    /**
     * @param PostSettings|array $source
     * @return array
     */
    public function overrideSettingsToRows($source)
    {
        $getValue = function ($source, $key) {
            $v = ArrayHelper::getValue($source, $key);
            if ($v === null) {
                $lowerKey = strtolower($key);
                if ($lowerKey != $key) {
                    $v = ArrayHelper::getValue($source, $lowerKey);
                }
            }
            return $v;
        };
        $rows = [];
        if (null !== $v = $getValue($source, 'position')) {
            $rows[] = [esc_html__('Position', 'lwptoc'), ArrayHelper::getValue(Core::$plugin->positionsList, $v)];
        }
        if (null !== $v = $getValue($source, 'min')) {
            $rows[] = [esc_html__('Minimal Count of Headers', 'lwptoc'), $v];
        }
        if (null !== $v = $getValue($source, 'depth')) {
            $rows[] = [esc_html__('Depth', 'lwptoc'), $v];
        }
        if (null !== $v = $getValue($source, 'hierarchical')) {
            $rows[] = [esc_html__('Hierarchical View', 'lwptoc'), $v ? __('Enabled', 'lwptoc') : __('Disabled', 'lwptoc')];
        }
        if (null !== $v = $getValue($source, 'numeration')) {
            $rows[] = [esc_html__('Numeration', 'lwptoc'), ArrayHelper::getValue(Core::$plugin->numerationsList, $v)];
        }
        if (null !== $v = $getValue($source, 'title')) {
            $rows[] = [esc_html__('Title', 'lwptoc'), $v == '' ? null : $v];
        }
        if (null !== $v = $getValue($source, 'toggle')) {
            $rows[] = [esc_html__('Toggle Show/Hide', 'lwptoc'), $v ? __('Enabled', 'lwptoc') : __('Disabled', 'lwptoc')];
        }
        if (null !== $v = $getValue($source, 'labelShow')) {
            $rows[] = [esc_html__('Label Show', 'lwptoc'), $v == '' ? null : $v];
        }
        if (null !== $v = $getValue($source, 'labelHide')) {
            $rows[] = [esc_html__('Label Hide', 'lwptoc'), $v == '' ? null : $v];
        }
        if (null !== $v = $getValue($source, 'hideItems')) {
            $rows[] = [esc_html__('Hide Items', 'lwptoc'), $v ? __('Enabled', 'lwptoc') : __('Disabled', 'lwptoc')];
        }
        if (null !== $v = $getValue($source, 'smoothScroll')) {
            $rows[] = [esc_html__('Smooth Scroll', 'lwptoc'), $v ? __('Enabled', 'lwptoc') : __('Disabled', 'lwptoc')];
        }
        if (null !== $v = $getValue($source, 'smoothScrollOffset')) {
            $rows[] = [esc_html__('Smooth Scroll Offset Top', 'lwptoc'), $v . 'px'];
        }
        if (null !== $v = $getValue($source, 'width')) {
            $rows[] = [esc_html__('Width', 'lwptoc'), Core::$plugin->widthToLabel($v)];
        }
        if (null !== $v = $getValue($source, 'float')) {
            $rows[] = [esc_html__('Float', 'lwptoc'), ArrayHelper::getValue(Core::$plugin->floatsList, $v)];
        }
        if (null !== $v = $getValue($source, 'titleFontSize')) {
            $rows[] = [esc_html__('Title Font Size', 'lwptoc'), Core::$plugin->fontSizeToLabel($v)];
        }
        if (null !== $v = $getValue($source, 'titleFontWeight')) {
            $rows[] = [esc_html__('Title Font Weight', 'lwptoc'), ArrayHelper::getValue(Core::$plugin->fontWeightsList, $v)];
        }
        if (null !== $v = $getValue($source, 'itemsFontSize')) {
            $rows[] = [esc_html__('Items Font Size', 'lwptoc'), Core::$plugin->fontSizeToLabel($v)];
        }
        if (null !== $v = $getValue($source, 'colorScheme')) {
            $rows[] = [esc_html__('Color Scheme', 'lwptoc'), ArrayHelper::getValue(Core::$plugin->colorSchemesList, $v)];
        }
        foreach ([
                     'backgroundColor' => 'Background Color',
                     'borderColor' => 'Border Color',
                     'titleColor' => 'Title Color',
                     'linkColor' => 'Link Color',
                     'hoverLinkColor' => 'Hover Link Color',
                     'visitedLinkColor' => 'Visited Link Color',
                 ] as $var => $label) {
            if (null !== $v = $getValue($source, $var)) {
                $rows[] = [esc_html__($label, 'lwptoc'), OverrideColorBadge::widget(['color' => $v]), false];
            }
        }
        if (null !== $v = $getValue($source, 'wrapNoindex')) {
            $rows[] = [esc_html__('Wrap table of contents with <!--noindex--> tag', 'lwptoc'), $v ? __('Enabled', 'lwptoc') : __('Disabled', 'lwptoc')];
        }
        if (null !== $v = $getValue($source, 'skipHeadingLevel')) {
            $rows[] = [esc_html__('Skip headings', 'lwptoc'), Core::$plugin->skipHeadingLevelToLabel($v)];
        }
        if (null !== $v = $getValue($source, 'skipHeadingText')) {
            $rows[] = [esc_html__('Skip headings', 'lwptoc'), $v == '' ? null : $v];
        }
        return array_map(function ($row) {
            if ($row[1] !== null && ArrayHelper::getValue($row, 2, true) === true) {
                $row[1] = esc_html($row[1]);
            }
            return $row;
        }, $rows);
    }
}
