<?php

namespace luckywp\tableOfContents\admin\widgets;

use luckywp\tableOfContents\core\base\Widget;

class OverrideColorBadge extends Widget
{

    /**
     * @var string
     */
    public $color;

    public function run()
    {
        return $this->color ? '<span class="lwptocColorBadge"><b style="background:' . $this->color . '"></b>' . $this->color . '</span>' : esc_html__('from scheme', 'lwptoc');
    }
}
