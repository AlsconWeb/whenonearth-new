<?php

use luckywp\tableOfContents\admin\widgets\fontSizeField\FontSizeField;
use luckywp\tableOfContents\admin\widgets\skipHeadingLevelField\SkipHeadingLevelField;
use luckywp\tableOfContents\admin\widgets\widthField\WidthField;
use luckywp\tableOfContents\core\admin\helpers\AdminHtml;
use luckywp\tableOfContents\admin\widgets\PostTypes;
use luckywp\tableOfContents\core\Core;
use luckywp\tableOfContents\core\helpers\Html;
use luckywp\tableOfContents\plugin\Settings;

return [

    // Основные настройки
    'general' => [
        'label' => esc_html__('General', 'lwptoc'),
        'sections' => [
            'main' => [
                'fields' => [

                    'min' => [
                        'label' => esc_html__('Minimal Count of Headers', 'lwptoc'),
                        'widget' => 'textInput',
                        'params' => [
                            'inputOptions' => [
                                'size' => AdminHtml::TEXT_INPUT_SIZE_SMALL,
                            ],
                        ],
                        'default' => 2,
                        'desc' => esc_html__('If the count of headers in the post is less, then table of contents is not displayed.', 'lwptoc'),
                    ],

                    'depth' => [
                        'label' => esc_html__('Depth', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->depthsList,
                        ],
                        'default' => 6,
                    ],

                    'hierarchical' => [
                        'label' => esc_html__('Hierarchical View', 'lwptoc'),
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('Enable', 'lwptoc'),
                            ],
                        ],
                        'default' => true,
                    ],

                    'numeration' => [
                        'label' => esc_html__('Numeration', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->numerationsList,
                        ],
                        'default' => 'decimalnested',
                    ],
                ],
            ],

            'header' => [
                'title' => esc_html__('Header', 'lwptoc'),
                'fields' => [

                    'title' => [
                        'label' => esc_html__('Title', 'lwptoc'),
                        'widget' => 'textInput',
                        'default' => __('Contents', 'lwptoc'),
                    ],

                    'toggle' => [
                        'label' => esc_html__('Toggle Show/Hide', 'lwptoc'),
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('Enable', 'lwptoc'),
                                'class' => 'js-lwptocToggleCheckbox',
                            ],
                        ],
                        'default' => true,
                    ],

                    'labelShow' => [
                        'label' => esc_html__('Label Show', 'lwptoc'),
                        'widget' => 'textInput',
                        'params' => [
                            'inputOptions' => [
                                'class' => 'js-lwptocToggleEl'
                            ],
                        ],
                        'default' => __('show', 'lwptoc'),
                    ],

                    'labelHide' => [
                        'label' => esc_html__('Label Hide', 'lwptoc'),
                        'widget' => 'textInput',
                        'params' => [
                            'inputOptions' => [
                                'class' => 'js-lwptocToggleEl'
                            ],
                        ],
                        'default' => __('hide', 'lwptoc'),
                    ],

                    'hideItems' => [
                        'label' => '',
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('By default, items of contents will be hidden', 'lwptoc'),
                                'class' => 'js-lwptocToggleEl',
                            ],
                        ],
                        'default' => false,
                    ],
                ],
            ],
            'behavior' => [
                'title' => esc_html__('Behavior', 'lwptoc'),
                'fields' => [

                    'smoothScroll' => [
                        'label' => esc_html__('Smooth Scroll', 'lwptoc'),
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('Enable', 'lwptoc'),
                                'class' => 'js-lwptocSmoothScrollCheckbox',
                            ],
                        ],
                        'default' => true,
                    ],

                    'smoothScrollOffset' => [
                        'label' => esc_html__('Scroll Offset Top', 'lwptoc'),
                        'widget' => 'textInput',
                        'params' => [
                            'inputOptions' => [
                                'size' => AdminHtml::TEXT_INPUT_SIZE_SMALL,
                                'class' => 'js-lwptocSmoothScrollIOffsetInput'
                            ],
                            'after' => ' px',
                        ],
                        'default' => 24,
                    ],
                ],
            ],
        ],
    ],

    // Внешний вид
    'appearance' => [
        'label' => esc_html__('Appearance', 'lwptoc'),
        'sections' => [
            'main' => [
                'fields' => [

                    'width' => [
                        'label' => esc_html__('Width', 'lwptoc'),
                        'widget' => function ($field) {
                            echo WidthField::widget([
                                'name' => $field['name'],
                                'value' => Core::$plugin->settings->getValue($field['group'], $field['id'], 'auto', false),
                            ]);
                        },
                        'sanitizeCallback' => [Settings::className(), 'sanitizeWidth'],
                        'default' => 'auto',
                    ],

                    'float' => [
                        'label' => esc_html__('Float', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->floatsList,
                        ],
                        'default' => 'none',
                    ],

                    'titleFontSize' => [
                        'label' => esc_html__('Title Font Size', 'lwptoc'),
                        'widget' => function ($field) {
                            echo FontSizeField::widget([
                                'name' => $field['name'],
                                'value' => Core::$plugin->settings->getValue($field['group'], $field['id'], 'default', false),
                                'defaultSize' => 100,
                            ]);
                        },
                        'sanitizeCallback' => [Settings::className(), 'sanitizeFontSize'],
                        'default' => 'default',
                    ],

                    'titleFontWeight' => [
                        'label' => esc_html__('Title Font Weight', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->fontWeightsList,
                        ],
                        'default' => 'bold',
                    ],

                    'itemsFontSize' => [
                        'label' => esc_html__('Items Font Size', 'lwptoc'),
                        'widget' => function ($field) {
                            echo FontSizeField::widget([
                                'name' => $field['name'],
                                'value' => Core::$plugin->settings->getValue($field['group'], $field['id'], 'default', false),
                                'defaultSize' => 90,
                            ]);
                        },
                        'sanitizeCallback' => [Settings::className(), 'sanitizeFontSize'],
                        'default' => '90%',
                    ],

                    'colorScheme' => [
                        'label' => esc_html__('Color Scheme', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->colorSchemesList,
                        ],
                        'default' => 'light',
                    ],
                ],
            ],
            'overrideColors' => [
                'title' => esc_html__('Override Color Scheme Colors', 'lwptoc'),
                'fields' => [

                    'backgroundColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Background Color', 'lwptoc'),
                    ],

                    'borderColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Border Color', 'lwptoc'),
                    ],

                    'titleColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Title Color', 'lwptoc'),
                    ],

                    'linkColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Link Color', 'lwptoc'),
                    ],

                    'hoverLinkColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Hover Link Color', 'lwptoc'),
                    ],

                    'visitedLinkColor' => [
                        'widget' => 'color',
                        'label' => esc_html__('Visited Link Color', 'lwptoc'),
                    ],
                ],
            ],
        ],
    ],

    // Автоматическая вставка
    'autoInsert' => [
        'label' => esc_html__('Auto Insert', 'lwptoc'),
        'sections' => [
            'main' => [
                'fields' => [

                    'enable' => [
                        'label' => esc_html__('Auto Insert Table of Contents', 'lwptoc'),
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('Enable', 'lwptoc'),
                                'class' => 'js-lwptocAutoInsertEnableCheckbox',
                            ],
                        ],
                        'default' => true,
                    ],

                    'position' => [
                        'label' => esc_html__('Position', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->positionsList,
                            'selectOptions' => [
                                'class' => 'js-lwptocAutoInsertEl',
                            ],
                        ],
                        'default' => 'beforefirstheading',
                    ],

                    'postTypes' => [
                        'label' => esc_html__('Post Types', 'lwptoc'),
                        'widget' => function ($field) {
                            echo PostTypes::widget([
                                'field' => $field,
                                'containerOptions' => [
                                    'class' => 'js-lwptocAutoInsertEl',
                                ],
                            ]);
                        },
                        'default' => ['post'],
                    ],
                ],
            ],
        ],
    ],

    // Обработка заголовков
    'processingHeadings' => [
        'label' => esc_html__('Processing Headings', 'lwptoc'),
        'sections' => [
            'main' => [
                'fields' => [
                    'postTypes' => [
                        'label' => esc_html__('Always for Post Types', 'lwptoc'),
                        'widget' => function ($field) {
                            echo PostTypes::widget([
                                'field' => $field,
                            ]);
                        },
                    ],
                ],
            ],
        ],
    ],

    // Прочее
    'misc' => [
        'label' => esc_html__('Misc.', 'lwptoc'),
        'sections' => [
            'main' => [
                'fields' => [
                    'hashFormat' => [
                        'label' => esc_html__('Hash Format', 'lwptoc'),
                        'widget' => 'select',
                        'params' => [
                            'items' => Core::$plugin->getHashFormatsList(),
                        ],
                        'default' => 'asheading',
                    ],

                    'wrapNoindex' => [
                        'label' => '',
                        'widget' => 'checkbox',
                        'params' => [
                            'checkboxOptions' => [
                                'label' => esc_html__('Wrap table of contents with <!--noindex--> tag', 'lwptoc'),
                            ],
                        ],
                        'default' => false,
                    ],
                ],
            ],
            'skipHeading' => [
                'title' => esc_html__('Skip Headings', 'lwptoc'),
                'fields' => [
                    'skipHeadingLevel' => [
                        'label' => esc_html__('By Level', 'lwptoc'),
                        'widget' => function ($field) {
                            echo SkipHeadingLevelField::widget([
                                'name' => $field['name'],
                                'value' => Core::$plugin->settings->getValue($field['group'], $field['id'], [], false),
                            ]);
                        },
                        'sanitizeCallback' => [Settings::className(), 'sanitizeSkipHeadingLevel'],
                    ],
                    'skipHeadingText' => [
                        'label' => esc_html__('By Text', 'lwptoc'),
                        'widget' => function ($field) {
                            $value = Core::$plugin->settings->getValue($field['group'], $field['id'], '', false);
                            echo Html::textarea($field['name'], Core::$plugin->skipHeadingTextToMultipleString((string)$value), [
                                'class' => 'regular-text',
                                'rows' => 5,
                            ]);
                            echo '<p class="description">';
                            echo esc_html__('Specify headings (one per line) to be excluded from the table of contents.', 'lwptoc');
                            echo '<br>';
                            echo sprintf(
                                esc_html__('Use an asterisk %s as a wildcard to match any text.', 'lwptoc'),
                                '<code>*</code>'
                            );
                            echo '</p>';
                        },
                        'sanitizeCallback' => function ($value) {
                            return Core::$plugin->skipHeadingTextMultipleStringToString((string)$value);
                        },
                    ],
                ],
            ],
            'postSettings' => [
                'title' => esc_html__('Post Settings', 'lwptoc'),
                'fields' => [
                    'showMetaboxPostTypes' => [
                        'label' => esc_html__('Show Panel "Table of Contents" in Post Types', 'lwptoc'),
                        'widget' => function ($field) {
                            echo PostTypes::widget([
                                'field' => $field,
                            ]);
                        },
                    ],
                ]
            ]
        ],
    ],
];
