msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-06-17 22:31+0300\n"
"PO-Revision-Date: 2019-06-17 22:33+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Poedit-KeywordsList: __;_;esc_html__\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin/editorBlock/src\n"

#: admin/Admin.php:34
msgid "Settings"
msgstr ""

#: admin/Admin.php:51 admin/Admin.php:52 admin/Admin.php:72 admin/Admin.php:99
#: admin/controllers/EditorBlockController.php:65
#: admin/controllers/ShortcodeController.php:68 plugin/WpWidget.php:20
msgid "Table of Contents"
msgstr ""

#: admin/Admin.php:100
msgid "Edit"
msgstr ""

#: admin/Admin.php:127 admin/forms/CustomizeForm.php:235
#: admin/widgets/customizeModal/views/modal.php:50 config/settings.php:285
msgid "Position"
msgstr ""

#: admin/Admin.php:130 admin/widgets/customizeModal/views/modal.php:73
#: config/settings.php:22
msgid "Minimal Count of Headers"
msgstr ""

#: admin/Admin.php:133 admin/forms/CustomizeForm.php:231
#: admin/widgets/customizeModal/views/modal.php:96 config/settings.php:34
msgid "Depth"
msgstr ""

#: admin/Admin.php:136 admin/widgets/customizeModal/views/modal.php:118
#: config/settings.php:43
msgid "Hierarchical View"
msgstr ""

#: admin/Admin.php:136 admin/Admin.php:145 admin/Admin.php:154
#: admin/Admin.php:157 admin/Admin.php:193
#: admin/widgets/customizeModal/views/modal.php:134
#: admin/widgets/customizeModal/views/modal.php:205
#: admin/widgets/customizeModal/views/modal.php:268
#: admin/widgets/customizeModal/views/modal.php:291
#: admin/widgets/customizeModal/views/modal.php:508
msgid "Enabled"
msgstr ""

#: admin/Admin.php:136 admin/Admin.php:145 admin/Admin.php:154
#: admin/Admin.php:157 admin/Admin.php:193
#: admin/widgets/customizeModal/views/modal.php:134
#: admin/widgets/customizeModal/views/modal.php:205
#: admin/widgets/customizeModal/views/modal.php:268
#: admin/widgets/customizeModal/views/modal.php:291
#: admin/widgets/customizeModal/views/modal.php:508
msgid "Disabled"
msgstr ""

#: admin/Admin.php:139 admin/forms/CustomizeForm.php:232
#: admin/widgets/customizeModal/views/modal.php:141 config/settings.php:54
msgid "Numeration"
msgstr ""

#: admin/Admin.php:142 admin/widgets/customizeModal/views/modal.php:163
#: config/settings.php:69
msgid "Title"
msgstr ""

#: admin/Admin.php:145 admin/widgets/customizeModal/views/modal.php:189
#: config/settings.php:75
msgid "Toggle Show/Hide"
msgstr ""

#: admin/Admin.php:148 admin/forms/CustomizeForm.php:233
#: admin/widgets/customizeModal/views/modal.php:212 config/settings.php:87
msgid "Label Show"
msgstr ""

#: admin/Admin.php:151 admin/forms/CustomizeForm.php:234
#: admin/widgets/customizeModal/views/modal.php:232 config/settings.php:98
msgid "Label Hide"
msgstr ""

#: admin/Admin.php:154
msgid "Hide Items"
msgstr ""

#: admin/Admin.php:157 admin/widgets/customizeModal/views/modal.php:275
#: config/settings.php:126
msgid "Smooth Scroll"
msgstr ""

#: admin/Admin.php:160 admin/widgets/customizeModal/views/modal.php:298
msgid "Smooth Scroll Offset Top"
msgstr ""

#: admin/Admin.php:163 admin/widgets/customizeModal/views/modal.php:322
#: config/settings.php:162
msgid "Width"
msgstr ""

#: admin/Admin.php:166 admin/forms/CustomizeForm.php:236
#: admin/widgets/customizeModal/views/modal.php:345 config/settings.php:174
msgid "Float"
msgstr ""

#: admin/Admin.php:169 admin/widgets/customizeModal/views/modal.php:367
#: config/settings.php:183
msgid "Title Font Size"
msgstr ""

#: admin/Admin.php:172 admin/forms/CustomizeForm.php:237
#: admin/widgets/customizeModal/views/modal.php:391 config/settings.php:196
msgid "Title Font Weight"
msgstr ""

#: admin/Admin.php:175 admin/widgets/customizeModal/views/modal.php:413
#: config/settings.php:205
msgid "Items Font Size"
msgstr ""

#: admin/Admin.php:178 admin/forms/CustomizeForm.php:238
#: admin/widgets/customizeModal/views/modal.php:437 config/settings.php:218
msgid "Color Scheme"
msgstr ""

#: admin/Admin.php:193 admin/widgets/customizeModal/views/modal.php:492
#: config/settings.php:352
msgid "Wrap table of contents with <!--noindex--> tag"
msgstr ""

#: admin/Admin.php:196 admin/Admin.php:199
msgid "Skip headings"
msgstr ""

#: admin/controllers/EditorBlockController.php:71
#: admin/controllers/ShortcodeController.php:74
#: admin/widgets/customizeModal/views/modal.php:567
#: admin/widgets/metabox/views/box.php:33
#: admin/widgets/widget/views/_override.php:18
msgid "empty"
msgstr ""

#: admin/views/rate/notice.php:9
msgid "Hello!"
msgstr ""

#: admin/views/rate/notice.php:12
#, php-format
msgid "We are very pleased that you are using the %s plugin within a few days."
msgstr ""

#: admin/views/rate/notice.php:16
msgid "Please rate plugin. It will help us a lot."
msgstr ""

#: admin/views/rate/notice.php:19
msgid "Rate the plugin"
msgstr ""

#: admin/views/rate/notice.php:26
msgid "Remind later"
msgstr ""

#: admin/views/rate/notice.php:32
msgid "I've already rated the plugin"
msgstr ""

#: admin/views/rate/notice.php:40
msgid "Thank you very much!"
msgstr ""

#: admin/views/settings/index.php:9
msgid "Table of Contents Settings"
msgstr ""

#: admin/views/settings/index.php:11
#, php-format
msgid "Leave a %s plugin review on WordPress.org"
msgstr ""

#: admin/widgets/OverrideColorBadge.php:17
msgid "from scheme"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:25
#: admin/widgets/customizeModal/views/modal.php:580
msgid "Cancel"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:26
msgid "Customize Table of Contents"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:40 config/settings.php:16
msgid "General"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:41 config/settings.php:156
msgid "Appearance"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:42 config/settings.php:334
msgid "Misc."
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:51
#: admin/widgets/customizeModal/views/modal.php:74
#: admin/widgets/customizeModal/views/modal.php:97
#: admin/widgets/customizeModal/views/modal.php:119
#: admin/widgets/customizeModal/views/modal.php:142
#: admin/widgets/customizeModal/views/modal.php:164
#: admin/widgets/customizeModal/views/modal.php:190
#: admin/widgets/customizeModal/views/modal.php:213
#: admin/widgets/customizeModal/views/modal.php:233
#: admin/widgets/customizeModal/views/modal.php:253
#: admin/widgets/customizeModal/views/modal.php:276
#: admin/widgets/customizeModal/views/modal.php:299
#: admin/widgets/customizeModal/views/modal.php:323
#: admin/widgets/customizeModal/views/modal.php:346
#: admin/widgets/customizeModal/views/modal.php:368
#: admin/widgets/customizeModal/views/modal.php:392
#: admin/widgets/customizeModal/views/modal.php:414
#: admin/widgets/customizeModal/views/modal.php:438
#: admin/widgets/customizeModal/views/modal.php:468
#: admin/widgets/customizeModal/views/modal.php:493
#: admin/widgets/customizeModal/views/modal.php:516
#: admin/widgets/customizeModal/views/modal.php:539
msgid "default"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:54
#: admin/widgets/customizeModal/views/modal.php:77
#: admin/widgets/customizeModal/views/modal.php:100
#: admin/widgets/customizeModal/views/modal.php:122
#: admin/widgets/customizeModal/views/modal.php:145
#: admin/widgets/customizeModal/views/modal.php:167
#: admin/widgets/customizeModal/views/modal.php:193
#: admin/widgets/customizeModal/views/modal.php:216
#: admin/widgets/customizeModal/views/modal.php:236
#: admin/widgets/customizeModal/views/modal.php:256
#: admin/widgets/customizeModal/views/modal.php:279
#: admin/widgets/customizeModal/views/modal.php:302
#: admin/widgets/customizeModal/views/modal.php:326
#: admin/widgets/customizeModal/views/modal.php:349
#: admin/widgets/customizeModal/views/modal.php:371
#: admin/widgets/customizeModal/views/modal.php:395
#: admin/widgets/customizeModal/views/modal.php:417
#: admin/widgets/customizeModal/views/modal.php:441
#: admin/widgets/customizeModal/views/modal.php:471
#: admin/widgets/customizeModal/views/modal.php:496
#: admin/widgets/customizeModal/views/modal.php:519
#: admin/widgets/customizeModal/views/modal.php:542
msgid "Click for override default value"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:86 config/settings.php:30
msgid ""
"If the count of headers in the post is less, then table of contents is not "
"displayed."
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:127
#: admin/widgets/customizeModal/views/modal.php:198
#: admin/widgets/customizeModal/views/modal.php:261
#: admin/widgets/customizeModal/views/modal.php:284
#: admin/widgets/customizeModal/views/modal.php:501 config/settings.php:47
#: config/settings.php:79 config/settings.php:130 config/settings.php:277
msgid "Enable"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:178
msgid "Without title"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:252 config/settings.php:113
msgid "By default, items of contents will be hidden"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:515
msgid "Skip heading by level"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:538
msgid "Skip heading by text"
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:552 config/settings.php:381
msgid ""
"Specify headings (one per line) to be excluded from the table of contents."
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:555 config/settings.php:384
#, php-format
msgid "Use an asterisk %s as a wildcard to match any text."
msgstr ""

#: admin/widgets/customizeModal/views/modal.php:583
msgid "Save"
msgstr ""

#: admin/widgets/customizeSuccess/views/widget.php:9
msgid "Saved!"
msgstr ""

#: admin/widgets/fontSizeField/views/widget.php:16 plugin/Plugin.php:266
msgid "Default"
msgstr ""

#: admin/widgets/fontSizeField/views/widget.php:17 plugin/Plugin.php:231
msgid "Custom Value"
msgstr ""

#: admin/widgets/metabox/views/box.php:14
msgid "The table of contents will be automatic added to this post."
msgstr ""

#: admin/widgets/metabox/views/box.php:18
msgid ""
"The headings will be processing in this post for use in Table of Contens in "
"widget or custom shortcode."
msgstr ""

#: admin/widgets/metabox/views/box.php:27
#: admin/widgets/widget/views/_override.php:12
msgid "Overridden settings:"
msgstr ""

#: admin/widgets/metabox/views/box.php:39
#: admin/widgets/widget/views/form.php:19
msgid "Customize"
msgstr ""

#: admin/widgets/metabox/views/box.php:44
msgid "Disable TOC"
msgstr ""

#: admin/widgets/metabox/views/box.php:49
msgid "Disable Processing"
msgstr ""

#: admin/widgets/metabox/views/box.php:61
msgid "Click \"Enable Processing\" for headings processing in this post."
msgstr ""

#: admin/widgets/metabox/views/box.php:64
msgid "Enable Processing"
msgstr ""

#: admin/widgets/metabox/views/box.php:72
msgid "Click \"Enable TOC\" for automatic add table of contents to this post."
msgstr ""

#: admin/widgets/metabox/views/box.php:75
msgid "Enable TOC"
msgstr ""

#: config/settings.php:65
msgid "Header"
msgstr ""

#: config/settings.php:71 plugin/Settings.php:88
msgid "Contents"
msgstr ""

#: config/settings.php:94 plugin/Settings.php:104
msgid "show"
msgstr ""

#: config/settings.php:105 plugin/Settings.php:112
msgid "hide"
msgstr ""

#: config/settings.php:122
msgid "Behavior"
msgstr ""

#: config/settings.php:138
msgid "Scroll Offset Top"
msgstr ""

#: config/settings.php:228
msgid "Override Color Scheme Colors"
msgstr ""

#: config/settings.php:233
msgid "Background Color"
msgstr ""

#: config/settings.php:238
msgid "Border Color"
msgstr ""

#: config/settings.php:243
msgid "Title Color"
msgstr ""

#: config/settings.php:248
msgid "Link Color"
msgstr ""

#: config/settings.php:253
msgid "Hover Link Color"
msgstr ""

#: config/settings.php:258
msgid "Visited Link Color"
msgstr ""

#: config/settings.php:267
msgid "Auto Insert"
msgstr ""

#: config/settings.php:273
msgid "Auto Insert Table of Contents"
msgstr ""

#: config/settings.php:297
msgid "Post Types"
msgstr ""

#: config/settings.php:315
msgid "Processing Headings"
msgstr ""

#: config/settings.php:320
msgid "Always for Post Types"
msgstr ""

#: config/settings.php:339
msgid "Hash Format"
msgstr ""

#: config/settings.php:360
msgid "Skip Headings"
msgstr ""

#: config/settings.php:363
msgid "By Level"
msgstr ""

#: config/settings.php:373
msgid "By Text"
msgstr ""

#: config/settings.php:396
msgid "Post Settings"
msgstr ""

#: config/settings.php:399
msgid "Show Panel \"Table of Contents\" in Post Types"
msgstr ""

#: core/admin/AdminController.php:46
msgid "Sorry, you are not allowed to access this page."
msgstr ""

#: core/validators/BooleanValidator.php:27
msgid "{attribute} must be either \"{true}\" or \"{false}\"."
msgstr ""

#: core/validators/RangeValidator.php:27
msgid "{attribute} is invalid."
msgstr ""

#: core/validators/RequiredValidator.php:16
msgid "{attribute} cannot be blank."
msgstr ""

#: plugin/Plugin.php:101
msgid "Without numeration"
msgstr ""

#: plugin/Plugin.php:102
msgid "Decimal numbers (nested)"
msgstr ""

#: plugin/Plugin.php:103
msgid "Decimal numbers"
msgstr ""

#: plugin/Plugin.php:104
msgid "Roman numbers (nested)"
msgstr ""

#: plugin/Plugin.php:105
msgid "Roman numbers"
msgstr ""

#: plugin/Plugin.php:115
msgid "Before first heading"
msgstr ""

#: plugin/Plugin.php:116
msgid "After first heading"
msgstr ""

#: plugin/Plugin.php:117
msgid "After first block (paragraph, list or heading)"
msgstr ""

#: plugin/Plugin.php:118
msgid "Top"
msgstr ""

#: plugin/Plugin.php:119
msgid "Bottom"
msgstr ""

#: plugin/Plugin.php:153
msgid "Thin"
msgstr ""

#: plugin/Plugin.php:154
msgid "Extra Light"
msgstr ""

#: plugin/Plugin.php:155
msgid "Light"
msgstr ""

#: plugin/Plugin.php:156
msgid "Normal"
msgstr ""

#: plugin/Plugin.php:157
msgid "Medium"
msgstr ""

#: plugin/Plugin.php:158
msgid "Semi Bold"
msgstr ""

#: plugin/Plugin.php:159
msgid "Bold"
msgstr ""

#: plugin/Plugin.php:160
msgid "Extra Bold"
msgstr ""

#: plugin/Plugin.php:161
msgid "Heavy"
msgstr ""

#: plugin/Plugin.php:190 plugin/Plugin.php:317
msgid "None"
msgstr ""

#: plugin/Plugin.php:191
msgid "Left"
msgstr ""

#: plugin/Plugin.php:192
msgid "Right"
msgstr ""

#: plugin/Plugin.php:193
msgid "Right without flow"
msgstr ""

#: plugin/Plugin.php:194
msgid "Center"
msgstr ""

#: plugin/Plugin.php:204
msgid "Light Colors"
msgstr ""

#: plugin/Plugin.php:205
msgid "Dark Colors"
msgstr ""

#: plugin/Plugin.php:206
msgid "White"
msgstr ""

#: plugin/Plugin.php:207
msgid "Transparent"
msgstr ""

#: plugin/Plugin.php:217
msgid "As heading (#Example_Heading_Text)"
msgstr ""

#: plugin/Plugin.php:218
msgid "Counter (#lpwtoc1, #lwptoc2, …)"
msgstr ""

#: plugin/Plugin.php:229
msgid "Auto"
msgstr ""

#: plugin/Plugin.php:230
msgid "Full Width"
msgstr ""

#: plugin/Plugin.php:393
msgid ""
"Creates a table of contents for your posts/pages. Works automatically or "
"manually (via shortcode, Gutenberg block or widget)."
msgstr ""
