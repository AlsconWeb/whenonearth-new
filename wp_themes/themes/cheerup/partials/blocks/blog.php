<?php

/**
 * Determine the listing style to use
 */
if (empty($type) OR $type == 'loop-default') {
	$type = Bunyad::options()->category_loop;
}

// loop template
$template = empty($type) ? 'loop' : $type;

/**
 * Process the block and setup the query
 * 
 * @var array  $atts  Shortcode attribs by Bunyad_Theme_ShortCodes::_render()
 * @var string $tag   Shortcode used, example: highlights
 * 
 * @see Bunyad_ShortCodes::__call()
 */
$block = new Bunyad_Theme_Block($atts, $tag);
// var_dump($block);
// die;
$query = $block->process()->query;

// Add VC design CSS class if needed
$extra_class = '';
if (!empty($css) && function_exists('vc_shortcode_custom_css_class')) {
	$extra_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $tag, $atts);
}

// Save current options so that can they can be restored later
$options = Bunyad::options()->get_all();

if (!empty($grid_style)) {
	Bunyad::options()->post_grid_style = $grid_style;
}

?>

	<section <?php Bunyad::markup()->attribs('blog-block', array('class' => array('cf block', $tag, $extra_class), 'data-id' => $block->block_id)); ?>>
	
		<?php echo $block->output_heading(); ?>
		
		<div class="block-content">
		<?php
		
			// Get our loop template with include to preserve local variable scope
			$data = compact('block', 'query', 'atts', 'grid_cols', 'grid_style', 'pagination', 'pagination_type', 'show_excerpt', 'excerpt_length', 'show_footer');
			Bunyad::get('helpers')->loop($template, $data);
		
		?>
		</div>
	
	</section>

<?php 

// Reset
wp_reset_postdata(); 

// Restore modified options
Bunyad::options()->set_all($options);
