<?php
/**
 * Process the block and setup the query
 * 
 * @var array  $atts  Shortcode attribs by Bunyad_Theme_ShortCodes::_render()
 * @var string $tag   Shortcode used, example: highlights
 * 
 * @see Bunyad_ShortCodes::__call()
 */
$block = new Bunyad_Theme_Block($atts, $tag);
$query = $block->process()->query;

// Add VC design CSS class if needed
$extra_class = '';
if (!empty($css) && function_exists('vc_shortcode_custom_css_class')) {
	$extra_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $tag, $atts);
}

?>

	<section <?php Bunyad::markup()->attribs('slider-block', array('class' => array('cf block', $tag, $extra_class), 'data-id' => $block->block_id)); ?>>
	
		<?php echo $block->output_heading(); ?>
		
		<div class="block-content">
		<?php
		
			/**
			 * Get slider template
			 */

			$template = 'partials/slider';
			if (!empty($type) && $type != 'default') {
				$template .= '-' . sanitize_file_name($type);
			}

			// Disable lazyload for slider
			(!Bunyad::options()->slider_parallax || Bunyad::lazyload()->disable());

			Bunyad::core()->partial($template, compact('query'));

			Bunyad::lazyload()->enable();
		
		?>
		</div>
	
	</section>

<?php wp_reset_postdata(); ?>