<?php
/**
 * Call CheerUo Advertisement widget
 */

if (!empty($code)) {
	$atts['ad_code'] =  rawurldecode(base64_decode($code));
}

$type = 'Bunyad_Ads_Widget';
$args = array();

$classes = "block";

if (!empty($css) && function_exists('vc_shortcode_custom_css_class')) {
	$classes .= apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $tag, $atts);
}

?>

<div class="<?php echo esc_attr($classes); ?>">
	<?php the_widget($type, $atts, $args); ?>
</div>

