<?php
/**
 * Handle migrations to newer version of themes
 */
class Bunyad_Theme_Admin_Migrations
{
	public $from_version;
	public $to_version;
	protected $options;

	public function __construct()
	{
		add_action('bunyad_theme_version_change', array($this, 'begin'));
	}

	/**
	 * Begin migration
	 */
	public function begin($from_version)
	{
		$this->from_version = $from_version;
		$this->to_version = Bunyad::options()->get_config('theme_version');

		// If from_version is empty, it's likely fresh install
		if (!$this->from_version) {
			return;
		}

		// Releases with an upgrader
		$releases = array(
			'6.0.0', 
			'5.1.0'
		);

		foreach ($releases as $index => $release) {

			// Downgrading perhaps?
			if (version_compare($this->to_version, $release, '<')) {
				continue;
			}

			// Current version is newer or already at it, stop.
			if (version_compare($this->from_version, $release, '>=')) {
				break;
			}
			
			$handler = array($this, 'migrate_' . str_replace('.', '', $release));
			if (is_callable($handler)) {
				call_user_func($handler);
			}
		}
	}

	/**
	 * Upgrade to version 6.0.0
	 */
	public function migrate_600()
	{
		// Fresh init to discard any leaky overrides
		Bunyad::options()->init();

		$this->options = get_option(Bunyad::options()->get_config('theme_prefix') .'_theme_options');

		/**
		 * Pre-6.0, featured_crop=1 was default. Respect the old defaults unless user had set something already.
		 */
		if (!isset($this->options['featured_crop'])) {
			$this->options['featured_crop'] = 1;
		}

		/**
		 * Color / customization settings with defaults changed
		 */
		$this->unset_if_match('css_footer_upper_bg', '#f7f7f7');
		$this->unset_if_match('css_footer_lower_bg', '#f7f7f7');

		// Font settings no longer have a default size
		$this->unset_if_match('css_font_post_body', array('font_size' => '14'));
		$this->unset_if_match('css_font_post_h1', array('font_size' => '25'));
		$this->unset_if_match('css_font_post_h2', array('font_size' => '23'));
		$this->unset_if_match('css_font_post_h3', array('font_size' => '20'));
		$this->unset_if_match('css_font_post_h4', array('font_size' => '18'));
		$this->unset_if_match('css_font_post_h5', array('font_size' => '16'));
		$this->unset_if_match('css_font_post_h6', array('font_size' => '14'));


		/**
		 * Search overlay was default earlier
		 */
		$options['search_style'] = 'overlay';

		// Save the changes
		Bunyad::options()
			->set_all($this->options)
			->update();

		// Flush CSS cache
		delete_transient('bunyad_custom_css_cache');
		wp_cache_flush();
	}

	/**
	 * Unset if a key exists in the options array with same value
	 */
	public function unset_if_match($key, $value)
	{
		if (!isset($this->options[$key])) {
			return;
		}

		if ($this->options[$key] == $value) {
			unset($this->options[$key]);
		}
		else if (is_array($value)) {

			$opt = &$this->options[$key];

			/**
			 * Remove for arrays of type 
			 */
			foreach ($value as $k => $v) {
				if (!is_string($k)) {
					continue;
				}
				
				// Remove if same
				if (!empty($opt[$k]) && $opt[$k] == $v) {
					unset($opt[$k]);
				}
			}

			if (empty($opt)) {
				unset($opt);
			}
		}
	}
}

// init and make available in Bunyad::get('theme_migrations')
Bunyad::register('theme_migrations', array(
	'class' => 'Bunyad_Theme_Admin_Migrations',
	'init'  => true
));