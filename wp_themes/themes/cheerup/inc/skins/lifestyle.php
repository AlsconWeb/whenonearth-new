<?php
/**
 * CheerUp Lifestyle Skin setup
 */
class Bunyad_Skins_Lifestyle
{
	public function __construct() 
	{
		// Add additional options
		$this->change_options();
		
		// Options are re-initialzed by init_preview, so need to be added again
		// @see Bunyad_Theme_Customizer::init_preview() 
		add_action('customize_preview_init', array($this, 'change_options'), 11);

		add_action('after_setup_theme', array($this, 'theme_init'), 13);
	}
	
	/**
	 * Add extra selectors needed for the skin
	 */
	public function change_options()
	{	
		$opts = Bunyad::options()->defaults;
		$opts['css_font_text']['css']['selectors'] .= ',
			.wp-caption-text,
			figcaption';
		
		// UI font
		$opts['css_font_secondary']['css']['selectors'] .= ', 
			.main-head.compact .posts-ticker a, 
			.list-post-b .read-more-btn,
			input[type="submit"], 
			button, input[type="button"], 
			.button,
			.author-box .author > a,
			.comments-list .comment-meta,
			.comments-list .comment-author,
			.single-magazine .cat-label a
		';

		$opts['css_main_color']['css']['selectors'] += array(
			'.sidebar .widget-title' => 'border-color: %s',
			'.main-head .dark .top-bar-content' => 'background-color: %1$s; border-color: %1$s',
			'.main-head.compact .dark .posts-ticker .heading' => 'color: %s',
		);
		
		
		// commit to options memory
		Bunyad::options()->defaults = $opts;
	}

	/**
	 * Run after main functions.php theme_init has run
	 */
	public function theme_init() 
	{
		Bunyad::posts()->more_text = esc_html__('Read More', 'cheerup');
	}
}

// init and make available in Bunyad::get('skins_content')
Bunyad::register('skins_lifestyle', array(
	'class' => 'Bunyad_Skins_Lifestyle',
	'init' => true
));