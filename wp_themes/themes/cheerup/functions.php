<?php
/**
 * CheerUp Theme!
 * 
 * This is the typical theme initialization file. Sets up the Bunyad Framework
 * and the theme functionality.
 * 
 * ----
 * 
 * Code Locations:
 * 
 *  /          -  WordPress default template files.
 *  lib/       -  Contains the Bunyad Framework files.
 *  inc/       -  Theme related functionality and some HTML helpers.
 *  admin/     -  Admin-only content.
 *  partials/  -  Template parts (partials) called via get_template_part().
 *  
 * Note: If you're looking to edit HTML, look for default WordPress templates in
 * top-level / and in partials/ folder.
 * 
 * Main Theme file:  inc/theme.php 
 */

// Already initialized?
if (class_exists('Bunyad_Core')) {
	return;
}

// Require PHP 5.3.2+
if (version_compare(phpversion(), '5.3.2', '<')) {

	function cheerup_php_notice() {
		$message = sprintf(esc_html_x('CheerUp requires %1$sPHP 5.3.2+%2$s. Please ask your webhost to upgrade to at least PHP 5.3.2. Recommended: %1$sPHP 7+%2$s%3$s', 'Admin', 'cheerup'), '<strong>', '</strong>', '<br>');
		printf('<div class="notice notice-error"><h3>Important:</h3><p>%1$s</p></div>', wp_kses_post($message));
	}

	add_action('admin_notices', 'cheerup_php_notice');	
	return;
}

// Initialize Framework
require_once get_theme_file_path('lib/bunyad.php');
require_once get_theme_file_path('inc/bunyad.php');

/**
 * Main Theme File: Contains most theme-related functionality
 * 
 * See file:  inc/theme.php
 */
require_once get_theme_file_path('inc/theme.php');

// Fire up the theme - make available in Bunyad::get('theme')
Bunyad::register('theme', array(
	'class' => 'Bunyad_Theme_Cheerup',
	'init' => true
));

// Legacy compat: Alias
Bunyad::register('cheerup', array('object' => Bunyad::get('theme')));

/**
 * Main Framework Configuration
 */
$bunyad_core = Bunyad::core()->init(apply_filters('bunyad_init_config', array(

	'theme_name'    => 'cheerup',
	'meta_prefix'   => '_bunyad',    // Keep meta framework prefix for data interoperability 
	'theme_version' => '6.0.3',

	// widgets enabled
	'widgets'      => array('about', 'posts', 'cta', 'ads', 'social', 'subscribe', 'social-follow', 'twitter', 'slider'),
	'widgets_type' => 'embed',
	'post_formats' => array('gallery', 'image', 'video', 'audio'),
	'customizer'   => true,
	
	// Enabled metaboxes and prefs - id is prefixed with _bunyad_ in init() method of lib/admin/meta-boxes.php
	'meta_boxes' => array(
		array('id' => 'post-options', 'title' => esc_html_x('Post Options', 'Admin: Meta', 'cheerup'), 'priority' => 'high', 'page' => array('post')),
		array('id' => 'page-options', 'title' => esc_html_x('Page Options', 'Admin: Meta', 'cheerup'), 'priority' => 'high', 'page' => array('page')),
	)
)));



/**
 *  Add Metabox Author 
 */

add_action('add_meta_boxes', 'metabox_author');
function metabox_author(){
	$screens = array('page');
	add_meta_box( 'myplugin_sectionid', 'Author choice', 'author_meta', $screens, 'side', 'core' );
}

/**
 * Creating a select with all available authors
 */
function author_meta($post, $meta){
	$screens = $meta['args'];
	$author_arr =  get_users(array('Author'));
	$author = [];
	foreach($author_arr as $val){
			$author[$val->data->display_name] =	$val->data->ID;
		
	}
	$author = array_merge(
		array(esc_html_x("Author", 'Admin', 'cheerup') => ''),
		$author
	);
	$select = '<select name="choice_author" class="choice_author">';
	foreach ($author as $key => $value) {
		$select .='<option value="'.$value.'">'.$key.'</option>';
	}
	$select .= '</select>';
	$select .= '<input type="hidden" class="authorID" value='.get_post_meta($post->ID, 'choice_author', true ).'>';

	echo $select;
}
/**
 * Updating metadata post
 */
add_action( 'save_post', 'save_postdata' );
function save_postdata( $post_id ){
	if ( ! isset( $_POST['choice_author'] ) )
		return;

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	if( ! current_user_can( 'edit_post', $post_id ) )
		return;
	
	$author_data = sanitize_text_field( $_POST['choice_author'] );
	update_post_meta( $post_id, 'choice_author', $author_data );
}


function add_select_action( ) {	
	wp_enqueue_script( 'select', get_template_directory_uri(). '/js/admin/select.js', array('jquery'), '1.0' );
	
}
add_action( 'admin_enqueue_scripts', 'add_select_action' );