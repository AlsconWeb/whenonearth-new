<?php
/**
 * Theme Options handler on admin side
 */
class Bunyad_Admin_Options
{
	public $option_key;
	public $options;
	
	public function __construct()
	{
		$this->option_key = Bunyad::options()->get_config('theme_prefix') .'_theme_options';

		// admin_menu comes before admin_init in order
		add_action('admin_menu', array($this, 'init'));
		
		// check theme version info
		add_action('admin_init', array($this, 'check_version'));
		add_action('after_switch_theme', array($this, 'check_version'));
		
		// Cleanup defaults. Caps will be pre-checked.
		add_action('customize_save_after', array($this, 'customizer_save_process'));
	}
	
	/**
	 * Register a single option using Setting API and use sanitization hook
	 */
	public function init()
	{
		// current user can edit?
		if (!current_user_can('edit_theme_options')) {
			return;
		}
	}
	
	/**
	 * Check current theme version and run an update hook if necessary
	 */
	public function check_version()
	{
		$option = Bunyad::options()->get_config('theme_prefix') . '_theme_version';

		// Stored version info
		$version_info   = (array) get_option($option);
		$stored_version = !empty($version_info['current']) ? $version_info['current'] : null;

		// Legacy compat: Get from options
		if (!$stored_version && Bunyad::options()->theme_version) {
			$stored_version = Bunyad::options()->theme_version;
		}
		
		// Update version if necessary
		if (!version_compare($stored_version, Bunyad::options()->get_config('theme_version'), '==')) {
			
			// Fire up the hook
			do_action('bunyad_theme_version_change', $stored_version);
			
			// Update the theme version
			$version_info['current'] = Bunyad::options()->get_config('theme_version');
				
			if ($stored_version) {
				$version_info['previous'] = $stored_version;
			}
			
			// updated changes in database
			update_option($option, array_filter($version_info));
		}
	}
	
	/**
	 * Load options locally for the class
	 */
	public function set_options($options = null)
	{
		if ($options) {
			$this->options = $options;
		}
		else if (!$this->options) { 
			
			// Get default options if empty
			$this->options = include get_template_directory() . '/admin/options.php';
		}
		
		return $this;
	}

	/**
	 * Extract elements/fields from the options hierarchy
	 * 
	 * @param array $options
	 * @param boolean $sub  add partial sub-elements in the list
	 * @param string  $tab_id  filter using a tab id
	 */
	public function get_elements_from_tree(array $options, $sub = false, $tab_id = null)
	{
		$elements = array();
		
		foreach ($options as $tab) {
			
			if ($tab_id != null && $tab['id'] != $tab_id) {
				continue;
			}
			
			foreach ($tab['sections'] as $section) 
			{
				foreach ($section['fields'] as $element) 
				{
					// pseudo element?
					if (empty($element['name'])) {
						continue;
					}
					
					$elements[$element['name']] = $element;
					
					// special treatment for typography section - it has sub-options
					if ($sub === true && $element['type'] == 'typography') {
						
						if (!empty($element['color'])) {
							// over-write 'value' key from the one in color - to set proper default
							$elements[$element['name'] . '_color'] = array_merge($element, $element['color']);
						}
						
						if (!empty($element['size'])) {
							$elements[$element['name'] . '_size'] = array_merge($element, $element['size']);
						}
					}
					
					// special treatment for typography section - it has sub-options
					if ($sub === true && $element['type'] == 'upload') {
						
						if (!empty($element['bg_type'])) {
							// over-write 'value' key from the one in color - to set proper default
							$elements[$element['name'] . '_bg_type'] = array_merge($element, $element['bg_type']);
						}
					}
					
				} // end fields loop
				
			} // end sections
		}
		
		return $elements;
	}

	/**
	 * Post-process Save customizer options.
	 * 
	 * This is needed to fix the defaults on customizer as it saves values in DB even when default is used.
	 */
	public function customizer_save_process()
	{
		Bunyad::options()->init();

		$elements = Bunyad::options()->defaults;
		$options  = get_option($this->option_key);

		if (empty($options)) {
			return;
		}
		
		// Remove defaults
		foreach ($options as $key => $value) {

			unset($default);

			// Default unspecified? Skip.
			if (isset($elements[$key])) {
				$default = $elements[$key]['value'];
			}

			/**
			 * For special arrays that have keys in options as social_profile[facebook]
			 */
			if (is_array($value)) {

				foreach ($value as $k => $v) {

					$ele_key = "{$key}[{$k}]";
					if (isset($elements[$ele_key])) {
						$ele_default = $elements[$ele_key]['value'];

						if ($ele_default == $v) {
							unset($options[$key][$k]);
						}
					}
				}

				// Remove empty array
				if (!count($options[$key])) {
					unset($options[$key]);
				}
			}
			
			// Remove default			
			if (isset($default) && $default == $value) {
				unset($options[$key]);
			}
		}

		// Save the updated options
		update_option($this->option_key, $options);
	}
	
	/**
	 * Delete / reset options - security checks done outside
	 */
	public function delete_options($type = null)
	{
		// get options object
		$options_obj = Bunyad::options();
		
		if ($type == 'colors') 
		{
			$elements = $this->get_elements_from_tree($this->options, true, 'options-style-color');
			
			// preserve this
			unset($elements['predefined_style']);
		}
		else 
		{
			$elements = $this->get_elements_from_tree($this->options, true);
		}
		
		// loop through all elements and reset them
		foreach ($elements as $key => $element) {
			$options_obj->set($key, null); // unset / reset
		}

		// save in database
		$options_obj->update();
		
		return true;
	}
	
}